#!/bin/sh
## Update s4m version e.g. for new release
newversion="$1"

currvers=`grep "S4M_SCRIPT_VERSION=" ./inc/s4m-settings.sh | cut -d'=' -f 2`
builddate=`grep "S4M_BUILD_DATE=" ./inc/s4m-settings.sh | cut -d'=' -f 2`
echo "Current version is: $currvers ($builddate)"; echo

if [ -z "$newversion" ]; then
  echo "To set a new version number: $0 <new_version>"; echo
  exit 1
fi

/bin/echo -n "Update s4m to v$newversion ? [y/N] "
read ans
if [ -z "$ans" ]; then
  ans="N"
fi
case $ans in
  y|Y)
    sed -i -r -e 's|S4M_SCRIPT_VERSION\=.*$|S4M_SCRIPT_VERSION="'$newversion'"|' ./inc/s4m-settings.sh
    datenow=`date +"%Y-%m-%d"`
    sed -i -r -e 's|S4M_BUILD_DATE\=.*$|S4M_BUILD_DATE="'$datenow'"|' ./inc/s4m-settings.sh
    ;;
esac
