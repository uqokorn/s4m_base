## S4M module utility functions
##
## NOTE: Not intended for direct execution.


### FUNCTIONS ###


## Function:    s4m_get_module_paths
##
## Return all canonical and user module library paths.
##
## Paths should be given in order of precedence i.e. currently, core modules from
## s4m_base should override user modules to ensure corrrect operation of s4m in
## case of module/version clashes.
##
## T#2561: TODO: Test
##
s4m_get_module_paths () {
  s4m_gmp_module_paths="$S4M_SCRIPT_HOME/modules
$S4M_MODULE_HOME"

  echo "$s4m_gmp_module_paths"
}


## Function:	s4m_is_module_version_spec
##
## Test if a string looks like a module name and version spec
## e.g. "module/version"
##
## @PORTABILITY : grep "-P" is Linux flag only
##
s4m_is_module_version_spec () {
  test_string="$1"
  ## Test for long format "<module>/<version>::<command>"
  echo "$test_string" | grep -P "^[A-Za-z0-9_]+/[0-9]+\.*[0-9]*\:\:.*$" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    return 0
  fi
  ## Otherwise, test short version "<module>/<version>"
  echo "$test_string" | grep -P "^[A-Za-z0-9_]+\/[0-9]+\.*[0-9]*.*$" > /dev/null 2>&1
  return $?
}


## Extract a module version number from module spec
##
## Test/s:	TODO
##
s4m_extract_module_version () {
  echo "$1" | sed -r -e 's|^.*\/([0-9]+\.*[0-9]+).*$|\1|'
}


## Extract a module/namespace name from module spec
##
## Test/s:	TODO
##
s4m_extract_module () {
  echo "$1" | sed -r -e 's|^([A-Za-z0-9_]+)[\/\:]+.*$|\1|'
}


## Remove a module version number from "module/<version>::command" input
##
## Test/s:	TODO
##
## @PORTABILITY : grep "-P" is Linux flag only
##
s4m_remove_module_version () {
  test_string="$1"
  echo "$test_string" | grep -P "^[A-Za-z0-9_]+/[0-9]+\.*[0-9]*\:\:.*$" > /dev/null 2>&1
  ## Remove from long format "<module>/<version>::<command>"
  if [ $? -eq 0 ]; then
    echo "$test_string" | sed -r -e 's|^([A-Za-z0-9_]+)\/[0-9\.]+\:\:(.*)$|\1\:\:\2|'
    return
  fi
  ## Remove from short format
  echo "$test_string" | sed -r -e 's|^([A-Za-z0-9_]+)\/[0-9\.]+.*$|\1|'
}


## Function:	s4m_get_modules
##
## If non-zero argument given e.g. "1", "all" or "true" then
## all module names regardless of enabled / disabled status
## will be returned.
##
## Test/s:	TODO
##
## T#2561: TODO: Test removal of module path arg, "$1" and new use of "s4m_get_module_paths()"
##
s4m_get_modules () {
  s4m_gm_all="$1"

  ## T#2561: Test
  ## Add s4mbase/modules to search path (core modules)
  s4m_gm_modulepaths=`s4m_get_module_paths`

  s4m_debug "s4m_get_modules(): Got module search paths: [$s4m_gm_modulepaths]"
 
  s4m_gm_modules=""

  for mp in $s4m_gm_modulepaths
  do
    for m in `ls "$mp/"`
    do
      s4m_check_module_exists "$m" "$mp" > /dev/null
      if [ $? -ne 0 ]; then
        continue
      fi
      if [ -z "$s4m_gm_all" ]; then
        ## NOTE: Checks only highest version of module found
        s4m_gm_enabled=`s4m_get_module_config_item "$m" Enabled "$mp"`
        if [ $s4m_gm_enabled != "1" ]; then
          continue
        fi
      fi
      ## module exists and is enabled, add to list
      if [ -z "$s4m_gm_modules" ]; then
        s4m_gm_modules="$m"
      else
        s4m_gm_modules="$s4m_gm_modules
$m"
      fi
    done
  done
  echo "$s4m_gm_modules"
}


## Function:	s4m_check_module_exists
##
## Check module validity (has at least one "module.ini" in a versioned
## subdirectory)
##
## If no module search path given, will search all canonical paths in order
## as returned by function "s4m_get_module_paths()"
##
## Test/s:	TODO
##
## TODO: T#2561: Test with and without path arg
##
s4m_check_module_exists () {
  ## Can take module version spec "module/version"
  s4m_cme_mod="$1"
  s4m_cme_modulepath="$2"

  s4m_cme_modulepaths=""

  if [ -z "$s4m_cme_modulepath" ]; then
    s4m_debug "Searching all module paths for module '$s4m_cme_mod'"
    s4m_cme_modulepaths=`s4m_get_module_paths`
  else
    s4m_cme_modulepaths="$s4m_cme_modulepath
"
  fi

  s4m_cme_found="" 
  for mp in $s4m_cme_modulepaths
  do
    if [ -z "$mp" ]; then
      continue
    fi
    s4m_cme_moddir="$mp/$s4m_cme_mod"
    if [ ! -d "$s4m_cme_moddir" ]; then
      s4m_debug "$s4m_cme_moddir is not a directory, skipping" 2
      continue
    fi
    ## skip any directory without a module.ini file (because it can't be a module)
    s4m_cme_found=`find -L "$s4m_cme_moddir/" -type f -name "module.ini" 2>/dev/null`
    if [ -z "$s4m_cme_found" ]; then
      if s4m_is_module_version_spec "$s4m_cme_mod"; then
        s4m_warn "$s4m_cme_moddir/module.ini does not exist, skipping"
      else
        s4m_warn "$s4m_cme_moddir/<version>/module.ini does not exist, skipping"
      fi
    else
      ## Print found module path
      echo "$s4m_cme_moddir"
      return 0
    fi
  done

  return 1
}


## Function:	s4m_get_module_usage
##
## Return formatted usage information for given module.
##
## Test/s:	TODO
##
## T#2561: TODO: Refactor to remove input module path?
##
s4m_get_module_usage () {
  ## Can take module version spec "module/version"
  s4m_gmu_mod="$1"
  s4m_gmu_modulepath="$2"

  if s4m_check_module_exists "$s4m_gmu_mod" "$s4m_gmu_modulepath" > /dev/null 2>&1; then
    ## print module usage
    s4m_gmu_cmd_opts=`s4m_return_one_module_command_options "$s4m_gmu_mod" "$s4m_gmu_modulepath"`

    if [ ! -z "$s4m_gmu_cmd_opts" ]; then
      echo "
  COMMANDS:
"
      ## start a new command section
      echo "$s4m_gmu_cmd_opts" | while read line
      do
        s4m_gmu_cmd=`s4m_cmdopt_extract_token "$line" 2`
        s4m_gmu_optname=`s4m_cmdopt_extract_token "$line" 3`
        s4m_gmu_optdesc=`s4m_cmdopt_extract_token "$line" 4`
        s4m_gmu_optreqd=`s4m_cmdopt_extract_token "$line" 5`
        s4m_gmu_optflag=`s4m_cmdopt_extract_token "$line" 6`

        if [ "$s4m_gmu_cmd" != "$s4m_gmu_currcmd" ]; then
          s4m_gmu_currcmd="$s4m_gmu_cmd"
          echo "      $s4m_gmu_currcmd <args>"; echo
        fi
        if [ "$s4m_gmu_optreqd" != "NOFLAGS" ]; then
          echo "         $s4m_gmu_optdesc"
          echo "         --$s4m_gmu_optname|-$s4m_gmu_optflag ($s4m_gmu_optreqd)
        "
        else
          echo "         ## Where <args>: $s4m_gmu_optdesc
"
        fi
      done
    fi
  else
    s4m_error "Failed to retrieve module usage information!"
    return 1
  fi
}


## Function:  s4m_validate_module_binary_dependencies
##
## Validate any binary dependencies for module (as specified in module.ini)
##
## Test/s:  TODO
##
## T#2031: Test
##
## T#2561: TODO: Test refactoring
##
s4m_validate_module_binary_dependencies () {
  ## Can be module/version spec
  s4m_vmbd_modulename="$1"
  s4m_vmbd_inipath="$2"

  s4m_debug "s4m_validate_module_binary_dependencies(): Got module=[$s4m_vmbd_modulename]" 2

  ## Discover module ini path if not given
  if [ -z "$s4m_vmbd_inipath" ]; then
    s4m_vmbd_inipath=`s4m_find_module_config_path "$s4m_vmbd_modulename"`
    s4m_debug "  Found INI path: $s4m_vmbd_inipath"
  fi

  s4m_vmbd_module_cmd_deps=`s4m_get_module_multiline_config_item "$s4m_vmbd_modulename" CommandDepends "$s4m_vmbd_inipath"`
  s4m_vmbd_module_cmd_depslist=`echo "$s4m_vmbd_module_cmd_deps" | tr "," "\n" | grep -P ":"`
  for d in $s4m_vmbd_module_cmd_depslist
  do
    s4m_vmbd_cmd=`echo "$d" | cut -d':' -f 1`
    s4m_vmbd_dep=`echo "$d" | cut -d':' -f 2`
    which "$s4m_vmbd_dep" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      s4m_error "Unsatisfied dependency '$s4m_vmbd_dep' of module/command '$s4m_vmbd_modulename::$s4m_vmbd_cmd'!
Please check environment and/or paths to dependent programs."
      return 1
    fi
  done
}


## Function:  s4m_print_qualifier_version_help
## 
## Print module version and qualifier format help.
##
## Test/s: TODO
##
s4m_print_qualifier_version_help () {
  s4m_log "Expected module version format: <X.Y> where 'X' and 'Y' are integers. Minor revision 'Y' is optional."
  s4m_log "Allowed qualifiers '>=' or '==' only."
}


## Function:  s4m_validate_module_version_dependencies
##
## Validate any module dependencies for module (as specified in module.ini)
##
## Test/s:  TODO
##
## T#2561: TODO: Test refactoring
##
s4m_validate_module_version_dependencies () {
  ## T#2031: Can be module/version spec
  s4m_mvd_modulename="$1"
  if [ -z "$s4m_mvd_modulename" ]; then
    s4m_mvd_modulename="$S4M_MODULE"
  fi
  s4m_mvd_inipath="$2"

  s4m_debug "s4m_validate_module_version_dependencies(): Got module=[$s4m_mvd_modulename]"

  ## Discover module ini path if not given
  if [ -z "$s4m_mvd_inipath" ]; then
    s4m_mvd_inipath=`s4m_find_module_config_path "$s4m_mvd_modulename"`
    s4m_debug "  Found INI path: $s4m_mvd_inipath"
  fi

  ## T#2561: Test ini path input
  s4m_mvd_module_vers_deps=`s4m_get_module_multiline_config_item "$s4m_mvd_modulename" ModuleDepends "$s4m_mvd_inipath"`
  s4m_mvd_module_vers_depslist=`echo "$s4m_mvd_module_vers_deps" | tr "," "\n"`
  for d in $s4m_mvd_module_vers_depslist
  do
    s4m_mvd_mod=`echo "$d" | sed -r -e 's|^([A-Za-z0-9_]+).*$|\1|'`
    s4m_mvd_vers=`echo "$d" | sed -r -e 's|^.*\=([0-9\.]+)$|\1|' 2> /dev/null`

    if [ $? -eq 0 -a ! -z "$s4m_mvd_vers" ]; then
      s4m_mvd_qualifier=`echo "$d" | sed -r -e 's|^[A-Za-z0-9_]+([\>\=]*[\=]*)[0-9\.]+$|\1|' 2> /dev/null`
      if ! echo "$s4m_mvd_qualifier" | grep -P "^[\>\=]+$" > /dev/null 2>&1; then
        s4m_error "Invalid qualifier string or version format '$s4m_mvd_qualifier'!"
        s4m_print_qualifier_version_help
        return 1
      fi
    else
      s4m_error "Failed to extract version dependency information for module '$s4m_mvd_modulename'! Check syntax:"
      s4m_print_qualifier_version_help
      return 1
    fi

    s4m_debug "s4m_validate_module_version_dependencies(): d=[$d], mod=[$s4m_mvd_mod], vers=[$s4m_mvd_vers], qualifier=[$s4m_mvd_qualifier]" 3

    s4m_check_module_exists "$s4m_mvd_mod" "$s4m_mvd_modulepath" > /dev/null
    if [ $? -ne 0 ]; then
      s4m_error "Required module dependency '$s4m_mvd_mod' was not found!"
      return 1
    fi
    if [ ! -z "$s4m_mvd_vers" ]; then
      s4m_check_module_version "$s4m_mvd_mod" "$s4m_mvd_vers" "$s4m_mvd_qualifier"
      if [ $? -ne 0 ]; then
        return 1
      fi
      s4m_log "Module dependency $s4m_mvd_mod $s4m_mvd_qualifier $s4m_mvd_vers was satisfied."
    fi
  done
}


## Function:  s4m_get_module_version
##
## Return version string for current or given module.
##
## Test/s:  TODO
##
## T#2561: TODO: Refactor to remove or make module path mandatory
##
s4m_get_module_version () {
  s4m_gmv_module="$1"
  s4m_gmv_modulepath="$2"
  if [ -z "$s4m_gmv_module" ]; then
    echo "$S4M_MOD_VERS"
    return
  fi
  if [ -z "$s4m_gmv_modulepath" ]; then
    s4m_gmv_modulepath="$S4M_MODULE_HOME"
  fi
  s4m_get_module_config_item "$s4m_gmv_module" Version "$s4m_gmv_modulepath"
}


## Function:  s4m_check_module_version
##
## Perform alpha-numeric version string match and/or greater-than-or-equal testing.
##
## Test/s:  TODO
##
## T#2561: TODO: Refactor to remove or make module path mandatory
##
s4m_check_module_version () {
  s4m_cmv_modulename="$1"
  s4m_cmv_version="$2"
  s4m_cmv_qualifier="$3"
  s4m_cmv_modulepath="$4"
  if [ -z "$s4m_cmv_modulepath" ]; then
    s4m_cmv_modulepath="$S4M_MODULE_HOME"
  fi

  s4m_cmv_foundvers=`s4m_get_module_config_item "$s4m_cmv_modulename" Version "$s4m_cmv_modulepath"`
  if [ -z "$s4m_cmv_foundvers" ]; then
    s4m_error "In module '$s4m_cmv_modulename' check for version $s4m_cmv_qualifier $s4m_cmv_version - found no Version string!"
    return 1
  fi

  ## Version string like <X.Y> where X and Y are integers. Zero is okay in either major or minor revision spec
  ## or major versions only without minor revision.
  if ! echo "$s4m_cmv_foundvers" | grep -P "^[0-9\.]+$" > /dev/null 2>&1; then
    s4m_error "Module '$s4m_cmv_modulename' Version specification ($s4m_cmv_foundvers) is invalid!"
    s4m_print_qualifier_version_help
    return 1
  fi

  if [ "$s4m_cmv_qualifier" = "==" ]; then
    if [ "$s4m_cmv_foundvers" = "$s4m_cmv_version" ]; then
      return 0
    else
      s4m_error "Current module requires $s4m_cmv_modulename $s4m_cmv_qualifier $s4m_cmv_version but installed version is $s4m_cmv_foundvers"
      return 1
    fi
  elif [ "$s4m_cmv_qualifier" = ">=" ]; then
    ## Compare major and minor versions separately
    ## If minor version is given, both major and minor must be >= target
    if echo "$s4m_cmv_version" | grep -P "\." > /dev/null 2>&1; then
      s4m_cmv_version_major=`echo "$s4m_cmv_version" | cut -d'.' -f 1`
      s4m_cmv_version_minor=`echo "$s4m_cmv_version" | cut -d'.' -f 2`
    else
      s4m_cmv_version_major="$s4m_cmv_version"
      s4m_cmv_version_minor=0
    fi
    if echo "$s4m_cmv_foundvers" | grep -P "\." > /dev/null 2>&1; then
      s4m_cmv_foundvers_major=`echo "$s4m_cmv_foundvers" | cut -d'.' -f 1`
      s4m_cmv_foundvers_minor=`echo "$s4m_cmv_foundvers" | cut -d'.' -f 2`
    else
      s4m_cmv_foundvers_major="$s4m_cmv_foundvers"
      s4m_cmv_foundvers_minor=0
    fi

    s4m_debug "  Need: module '$s4m_cmv_modulename' version $s4m_cmv_qualifier $s4m_cmv_version_major.$s4m_cmv_version_minor" 3
    s4m_debug "  Found: module '$s4m_cmv_modulename' version $s4m_cmv_foundvers_major.$s4m_cmv_foundvers_minor" 3

    ## "bc" evaluates expression and echos "0" for false, "1" for true
    ## So we negate the expression and interpret in the traditional shell
    ## way - "0" for true, "1" for false
    majorisgreaterorequal=`echo "! $s4m_cmv_foundvers_major >= $s4m_cmv_version_major" | bc -l 2> /dev/null`
    if [ ! -z "$s4m_cmv_foundvers_minor" -a ! -z "$s4m_cmv_version_minor" ]; then
      minorisgreaterorequal=`echo "! $s4m_cmv_foundvers_minor >= $s4m_cmv_version_minor" | bc -l 2> /dev/null`
    fi
    if [ -z "$minorisgreaterorequal" ]; then
      if [ $majorisgreaterorequal -ne 0 ]; then
        s4m_error "Current module requires $s4m_cmv_modulename $s4m_cmv_qualifier $s4m_cmv_version but installed version is $s4m_cmv_foundvers"
      else
        s4m_debug "Validated major versions only: $s4m_cmv_modulename $s4m_cmv_qualifier $s4m_cmv_version, returning 0" 3
        return 0
      fi
    else
      if [ $majorisgreaterorequal -ne 0 -o $minorisgreaterorequal -ne 0 ]; then
        s4m_error "Current module requires $s4m_cmv_modulename $s4m_cmv_qualifier $s4m_cmv_version but installed version is $s4m_cmv_foundvers"
      else
        s4m_debug "Validated major AND minor versions: $s4m_cmv_modulename $s4m_cmv_qualifier $s4m_cmv_version, returning 0" 3
        return 0
      fi
    fi

  else
    s4m_error "An error occurred in module '$s4m_cmv_modulename' version checking."
    s4m_print_qualifier_version_help
    return 1
  fi

  return 1
}


## Examples:
##   0.2  > 0.1
##   0.11 > 0.1
##   0.11 > 0.2
##   1.10 > 1.2
##
## NOTE: Currently only supported format is "<major>.<minor>" for version numbers
##
## PORTABILITY:
##	grep -P usage
##
s4m_get_max_module_version () {
  ## e.g. $S4M_MODULE_HOME/<module_name>
  s4m_gmmv_module_dir="$1"

  if [ ! -d "$s4m_gmmv_module_dir" ]; then
    s4m_error "Module directory '$s4m_gmmv_module_dir' not found!"
    return 1
  fi
  s4m_gmmv_vers_list=`ls -1 "$s4m_gmmv_module_dir/"`
  if [ -z "$s4m_gmmv_vers_list" ]; then
    s4m_error "No versioned module instances found in path '$s4m_gmmv_module_dir'!"
    return 1
  fi

  ## Retain only things that look like version numbers "X.Y" or simply "X"
  s4m_gmmv_clean_vers=`echo "$s4m_gmmv_vers_list" | grep -P "[0-9]+\.*[0-9+]*"`
  if [ -z "$s4m_gmmv_vers_list" ]; then
    s4m_error "No valid module versions found in path '$s4m_gmmv_module_dir'!"
    return 1
  fi
  ## Do a "version" sort (ascending) and return bottom (largest) value
  ## NOTE: Not using "-V" flag because not available in older GNU sort versions (e.g. 5.97)
  ##   Stemformatics modules don't use 3 version levels e.g. x.y.z, but in case somebody
  ##   out there wants to, then this would allow the versions to be sorted correctly.
  echo "$s4m_gmmv_clean_vers" | sort -n -t. -k1,1 -k2,2 -k3,3 | tail -1
}


## TODO: T#2561: Test (NEW!)
##
## Return the first module config (module.ini) path for a given module,
## by searching (in order of priority), all known module search paths
##
## NOTE: Assumes we've checked for module existence first!
##
s4m_find_module_config_path () {
  ## Simple module name or "module/<version>" format
  s4m_fmcp_module_spec="$1"

  s4m_fmcp_modulepaths=`s4m_get_module_paths`

  s4m_fmcp_inifile=""
  for mp in $s4m_fmcp_modulepaths
  do
    ## Retain the first valid module.ini path we come across (consistent with "s4m_get_module_paths()" expectations)
    if [ -z "$s4m_fmcp_inifile" ]; then
      s4m_fmcp_inifile=`s4m_get_module_config_path "$s4m_fmcp_module_spec" "$mp"`
      if [ ! -f "$s4m_fmcp_inifile" ]; then
        s4m_fmcp_inifile=""
      fi
    fi
  done
  ## If we didn't find a valid module.ini, exit with error (we shouldn't really have gotten this far, though)
  if [ -z "$s4m_fmcp_inifile" ]; then
    s4m_fatal_error "Could not determine path to module.ini for module '$s4m_fmcp_module_spec'"
  fi

  echo "$s4m_fmcp_inifile"
}


## Get a resolved path to a module config (module.ini) for a given module search path
## and module name/spec.
##
## PORTABILITY: grep -P usage
##
## TODO: T#2031: Test (module/version spec)
## TODO: T#2561: Test
##
s4m_get_module_config_path () {
  ## Simple module name or "module/<version>" format
  s4m_gmcp_module_spec="$1"
  ## Optional module home override
  s4m_gmcp_module_path="$2"
  ## T#2561: Changed to mandatory arg - exit with error if no path given
  if [ -z "$s4m_gmcp_module_path" ]; then
    s4m_fatal_error "s4m_get_module_config_path(): No module search path was given!"
  fi

  s4m_gmcp_ret=""

  ## Extract module name and version
  echo "$s4m_gmcp_module_spec" | grep -P "\/[0-9]+[0-9\.]*$" > /dev/null 2>&1
  s4m_gmcp_slash_delim=$?
  if [ $s4m_gmcp_slash_delim -eq 0 ]; then
    s4m_gmcp_module_name=`echo "$s4m_gmcp_module_spec" | cut -d'/' -f 1`
    s4m_gmcp_module_vers=`echo "$s4m_gmcp_module_spec" | cut -d'/' -f 2`
    if [ -f "$s4m_gmcp_module_path/$s4m_gmcp_module_name/$s4m_gmcp_module_vers/module.ini" ]; then
      s4m_gmcp_ret="$s4m_gmcp_module_path/$s4m_gmcp_module_name/$s4m_gmcp_module_vers/module.ini"
    else
      s4m_fatal_error "Could not find module INI: "$s4m_gmcp_module_path/$s4m_gmcp_module_name/$s4m_gmcp_module_vers/module.ini""
    fi

  ## Just module name, get path to newest version of module
  else
    if [ -d "$s4m_gmcp_module_path/$s4m_gmcp_module_spec" ]; then
      s4m_gmcp_max_vers=`s4m_get_max_module_version "$s4m_gmcp_module_path/$s4m_gmcp_module_spec"`
      s4m_debug "Got module '$s4m_gmcp_module_spec' max version=[$s4m_gmcp_max_vers]" 2
      s4m_gmcp_ret="$s4m_gmcp_module_path/$s4m_gmcp_module_spec/$s4m_gmcp_max_vers/module.ini"
    fi
  fi
  echo "$s4m_gmcp_ret"
}


## Function:  s4m_get_module_multiline_config_item
##
## Get multiline value defined in given module's config file
##
## NOTE: Assumes input module exists
##
## Test/s:  TODO
##
## T#2561: TODO: Test module path vs module ini path input
##
s4m_get_module_multiline_config_item () {
  ## Can take module version spec "module/version"
  s4m_gmmci_modulename="$1"
  s4m_gmmci_varname="$2"
  ## Optional module path OR INI path override
  s4m_gmmci_modulepath="$3"

  s4m_gmmci_moduleini=""
  ## Search canonical module paths for ini file
  if [ -z "$s4m_gmmci_modulepath" ]; then
    s4m_gmmci_moduleini=`s4m_find_module_config_path "$s4m_gmmci_modulename"`
    if [ $? -ne 0 ]; then
      return 1
    fi

  ## modulepath arg is a file, assume INI
  elif [ -f "$s4m_gmmci_modulepath" ]; then
    echo "$s4m_gmmci_modulepath" | grep -i "module\.ini" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      s4m_warn "Got a file path, assuming is module.ini BUT has unexpected name!"
    fi
    s4m_gmmci_moduleini="$s4m_gmmci_modulepath"

  ## modulepath arg is a directory, assume modules path
  elif [ -d "$s4m_gmmci_modulepath" ]; then
    s4m_debug "Got a directory path [$s4m_gmmci_modulepath], assuming is modules path" 2
    s4m_gmmci_moduleini=`s4m_get_module_config_path "$s4m_gmmci_modulename" "$s4m_gmmci_modulepath"`
    s4m_debug "  Using INI file path: $s4m_gmmci_moduleini" 2
  fi
  s4m_inifile_get_multiline "$s4m_gmmci_moduleini" "$s4m_gmmci_varname"
}


## Function:  s4m_get_module_config_item
##
## Get single line value defined in given module's config file
##
## Test/s:  TODO
##
## TODO: T#2031 Test
## TODO: T#2561: Test mandatory module path override
##
s4m_get_module_config_item () {
  ## Can take module version spec "module/version"
  ## If just module name given, returns config for highest version available
  s4m_gmci_modulename="$1"
  s4m_gmci_varname="$2"
  ## Optional module path override
  s4m_gmci_modulepath="$3"

  if [ -z "$s4m_gmci_modulepath" ]; then
    s4m_fatal_error "s4m_get_module_config_item(): Module path must be supplied!"
  fi
 
  s4m_gmci_moduleini=`s4m_get_module_config_path "$s4m_gmci_modulename" "$s4m_gmci_modulepath"`
  s4m_inifile_get "$s4m_gmci_moduleini" "$s4m_gmci_varname"
}


## Function:  s4m_return_modules_command_registry
##
## Find S4M modules, parse command information from their module.ini files
## and return to STDOUT as list of:
##
##        <module>:<namespace>:<command>:<target_script>
##
## NOTE: Only metadata for ENABLED modules are returned.
##
## Test/s:  TODO
##
## TODO: T#2561: Test code update for 's4m_get_module_paths()'
##
s4m_return_modules_command_registry () {
  s4m_rmcr_modulepaths=`s4m_get_module_paths`

  S4M_RMCR_CMD_REGISTRY=""

  for mp in $s4m_rmcr_modulepaths
  do
    ## List of found module directory names
    s4m_rmcr_dirlist=`find "$mp/" -maxdepth 1 -type d | grep "[A-Za-z0-9_\-]" | sed -r -e 's|^\.\/||'`

    for mpath in $s4m_rmcr_dirlist
    do
      mname=`basename "$mpath"`
      ## TODO: T#2561: Test
      s4m_check_module_exists "$mname" "$mp" > /dev/null 2>&1
      if [ $? -ne 0 ]; then
	continue
      fi
      ## extract command list for module
      s4m_rmcr_mroutes=`s4m_get_module_multiline_config_item "$mname" CommandRoutes "$mp"`
      s4m_rmcr_mnamespace=`s4m_get_module_config_item "$mname" Namespace "$mp"`
      s4m_rmcr_menabled=`s4m_get_module_config_item "$mname" Enabled "$mp"`

      s4m_debug "Module search path:  $mp" 2
      s4m_debug "Found module $mname (routes=[$s4m_rmcr_mroutes] namespace=[$s4m_rmcr_mnamespace] enabled=[$s4m_rmcr_menabled])" 2

      ## Skip processing disabled module
      if [ "$s4m_rmcr_menabled" != 1 ]; then
	continue
      fi

      ## build list of module:namespace:command:script entries so we know which
      ## S4M command arg (and opts) to route to which module/script
      s4m_rmcr_mroutes=`echo "$s4m_rmcr_mroutes" | tr "," "\n" | sed -e 's/\n\n/\n/g'`
      for r in $s4m_rmcr_mroutes
      do
	s4m_rmcr_cmd=`echo "$r" | cut -d':' -f 1`
	s4m_rmcr_target=`echo "$r" | cut -d':' -f 2 | tr -d [:cntrl:]`
	## check for duplicate namespace:command
	echo "$S4M_RMCR_CMD_REGISTRY" | grep ":$s4m_rmcr_mnamespace:$s4m_rmcr_cmd:" > /dev/null 2>&1
	if [ $? -eq 0 ]; then
	  s4m_error "Module '$mname' defines non-unique command '$s4m_rmcr_mnamespace::$s4m_rmcr_cmd', ignoring.."
	  continue
	fi
	S4M_RMCR_CMD_REGISTRY="$S4M_RMCR_CMD_REGISTRY
$mname:$s4m_rmcr_mnamespace:$s4m_rmcr_cmd:$s4m_rmcr_target
"
      done
      ## keep only non blank lines
      S4M_RMCR_CMD_REGISTRY=`echo "$S4M_RMCR_CMD_REGISTRY" | grep -P "[A-Za-z]+"`

      s4m_debug "Command registry :=
$S4M_RMCR_CMD_REGISTRY" 3
    done
  done
  echo "$S4M_RMCR_CMD_REGISTRY"
}


## TODO: T#2561: Commented out because this function isn't used anywhere.
## TODO: Remove ASAP when confirmed
## Function:  s4m_return_modules_command_options
##
## Return command options for multiple modules.
## See related "s4m_return_one_module_command_options()" function.
##
## Test/s:  TODO
##
#s4m_return_modules_command_options () {
#  s4m_rmco_modulepath="$1"
#  ## Set a default module path if none given
#  if [ ! -d "$s4m_rmco_modulepath" ]; then
#    s4m_rmco_modulepath="$S4M_MODULE_HOME"
#  fi
#  s4m_rmco_dirlist=`ls "$s4m_rmco_modulepath/"`
#
#  S4M_RMCO_CMD_OPT_REGISTRY=""
#  for m in $s4m_rmco_dirlist
#  do
#    s4m_rmco_modopt=`s4m_return_one_module_command_options "$m" "$s4m_rmco_modulepath"`
#    S4M_RMCO_CMD_OPT_REGISTRY="$S4M_RMCO_CMD_OPT_REGISTRY
#$s4m_rmco_modopt"
#  done
#  ## return command registry minus blank lines
#  echo "$S4M_RMCO_CMD_OPT_REGISTRY" | grep "\w"
#}


## Function:	s4m_return_one_module_command_options
##
## Extract options list from this module's .ini file.
## Can override the module search path with second arg.
##
## Return registry of module command options, basically in format specified in
## module.ini except with module name prepended:
## 
##    <modulename>:<command>:<optname>:<optdesc>:<required>:<shortflag>,
##
## Returns:	Prints parsed module commands to STDOUT, or:
##
##		Non-zero status on module error (not exists, not enabled).
##
## !!! WARNING !!!
##
## Returned command option lines are NOT directly compatible with the
##   "s4m_cmd_opt_extract_<fieldname>"  suite of helper functions!
## If wishing to use in this fashion, cut the modulename token off the
## front first!
##
## TODO: T#2561: Test mandatory path arg requirement
##
s4m_return_one_module_command_options () {
  ## Can take module version spec "module/version"
  s4m_romco_module="$1"
  s4m_romco_modulepath="$2"
  if [ -z "$s4m_romco_modulepath" -o ! -d "$s4m_romco_modulepath" ]; then
    s4m_fatal_error "s4m_return_one_module_command_options(): Given module path is empty or invalid!"
  fi

  ## TODO: T#2561: Check/update
  s4m_check_module_exists "$s4m_romco_module" "$s4m_romco_modulepath" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    return 1
  fi
  if [ -d "$s4m_romco_modulepath/$s4m_romco_module" ]; then
    s4m_romco_menabled=`s4m_get_module_config_item "$s4m_romco_module" Enabled "$s4m_romco_modulepath"`
    if [ $s4m_romco_menabled != "1" ]; then
      return 1
    fi
  fi
  ## extract command list for module
  s4m_romco_modopts=`s4m_get_module_multiline_config_item "$s4m_romco_module" CommandOptions "$s4m_romco_modulepath" | grep "\w" | sed "s|^|$s4m_romco_module=:|"`
  if [ -z "$s4m_romco_modopts" ]; then
    /bin/echo -n ""
    return 1
  fi
  /bin/echo -n "$s4m_romco_modopts"
}


## Function:  s4m_is_noflags_command
##
## Check whether a command is speciifed as 'NOFLAGS' in module.ini
##
## Returns: 0 if true, 1 if false
##
## Test/s:  TODO
##
s4m_is_noflags_command () {
  ## CommandOptions line from module.ini
  s4m_inc_optline="$1"
  s4m_inc_cmd="$2"

  s4m_inc_cmdrequired=`s4m_cmdopt_extract_required "$s4m_inc_optline"`
  if [ $s4m_inc_cmdrequired = "NOFLAGS" ]; then
    return 0
  fi
  return 1
}


## Function:  s4m_validate_module_command_options
##
## Validate module command options (if options spec defined in target module's module.ini file)
## Checks that module author-supplied "MANDATORY" options have been supplied by the user of this
## script.
##
## Test/s:  TODO
##
## Task #2031: IN PROGRESS
## 	* Adding support for $s4m_vmco_module specified as "module/version" 
##
## TODO: T#2561: Check/add all module search paths, test changes
##
s4m_validate_module_command_options () {
  ## Can be module/<version> format
  s4m_vmco_module="$1"
  shift
  s4m_vmco_cmd="$1"
  shift
  s4m_vmco_opts="$@"

  ## If command given as namespace::command, remove namespace part
  echo "$s4m_vmco_cmd" | grep "::" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    s4m_vmco_cmd=`echo "$s4m_vmco_cmd" | sed -r -e 's|^[A-Za-z0-9_]+\:\:||'`
  fi

  s4m_debug "s4m_validate_module_command_options(): Got module=[$s4m_vmco_module], cmd=[$s4m_vmco_cmd], opts=[$s4m_vmco_opts]" 3

  ## TODO: T#2561: Test
  s4m_vmco_modulepaths=`s4m_get_module_paths`

  s4m_vmco_inifile=""
  for mp in $s4m_vmco_modulepaths
  do
    ## Retain the first valid module.ini path we come across (consistent with "s4m_get_module_paths()" expectations)
    if [ -z "$s4m_vmco_inifile" ]; then
      s4m_vmco_inifile=`s4m_get_module_config_path "$s4m_vmco_module" "$mp"`
      if [ ! -f "$s4m_vmco_inifile" ]; then
        s4m_vmco_inifile=""
      fi
    fi
  done
  ## If we didn't find a valid module.ini, exit with error (we shouldn't really have gotten this far, though)
  if [ -z "$s4m_vmco_inifile" ]; then
    s4m_fatal_error "Could not determine path to module.ini for module '$s4m_vmco_module'"
  fi

  ## Get command opts from module.ini for this command
  ## T#2031: Updated
  s4m_vmco_modopts=`s4m_inifile_get_multiline "$s4m_vmco_inifile" CommandOptions | grep "^${s4m_vmco_cmd}\:"`

  s4m_debug "s4m_validate_module_command_options(): Got modopts=[$s4m_vmco_modopts]" 3

  ## A valid command must be specified in module.ini CommandOptions
  if [ ! -z "$s4m_vmco_modopts" ]; then
    echo "$s4m_vmco_modopts" | while read s4m_vmco_optline
    do 
      if [ ! -z "$s4m_vmco_optline" ]; then
        ## If command is marked as "NOFLAGS" then skip any further validation
        if s4m_is_noflags_command "$s4m_vmco_optline" "$s4m_vmco_cmd"; then
          s4m_debug "Command "$s4m_vmco_cmd" is a 'NOFLAGS' command, skipping further validation.." 2
          continue
        fi
        ## .. otherwise, any flags must be specified
        s4m_vmco_optname=`s4m_cmdopt_extract_optname "$s4m_vmco_optline"`
        s4m_vmco_optflagshort=`s4m_cmdopt_extract_shortflag "$s4m_vmco_optline"`

        s4m_debug "s4m_validate_module_command_options(): Parsing option [$s4m_vmco_optname] with flag [$s4m_vmco_optflagshort].." 3

        ## check for presence of mandatory option by long (name) or short flag
        ## T#2031: Updated
        ## TODO: T#2561: Test
        s4m_is_mandatory_module_cmd_option "$s4m_vmco_inifile" "$s4m_vmco_cmd" "$s4m_vmco_optname"
        if [ $? -eq 0 ]; then
          ## .. check flag was supplied
          s4m_vmco_optslist=`echo "$s4m_vmco_opts" | sed 's| \+|\n|g'`
          echo "$s4m_vmco_optslist" | grep -P "^(-$s4m_vmco_optflagshort|--$s4m_vmco_optname)$" > /dev/null 2>&1
          if [ $? -ne 0 -o -z "$s4m_vmco_opts" ]; then
            s4m_error "Option '$s4m_vmco_optname' (flag '-$s4m_vmco_optflagshort') is REQUIRED for command $s4m_vmco_module::$s4m_vmco_cmd"
            return 1
          fi
          ## .. check flag had a value (non-empty string) supplied
          s4m_vmco_val=`s4m_get_cmd_opts_flag_value "$s4m_vmco_optname" "$s4m_vmco_optflagshort" "$s4m_vmco_optslist"`
          if [ -z "$s4m_vmco_val" ]; then
            s4m_error "Value missing for option '$s4m_vmco_optname' (flag '-$s4m_vmco_optflagshort') in command $s4m_vmco_module::$s4m_vmco_cmd"
            return 1
          fi
        fi
      fi
    done

  ## Should not get here if we validated command prior..
  else
    s4m_error "Bad module command '$s4m_vmco_module::$s4m_vmco_cmd', failed to validate command options"
    return 1
  fi
}


## Function:  s4m_is_mandatory_module_cmd_option
##
## Test whether a given module command option is mandatory
## Returns: 0 if true, 1 if false.
##
## Test/s:  TODO
##
## T#2031: IN PROGRESS
## 	* Update for "module/version" spec
##
## TODO: T#2561: Test changes (working on module.ini path, not requiring module path)
##
s4m_is_mandatory_module_cmd_option () {
  s4m_immco_inifile="$1"
  s4m_immco_cmd="$2"
  s4m_immco_optname="$3"

  s4m_immco_modopts=`s4m_inifile_get_multiline "$s4m_immco_inifile" CommandOptions`
  ## Should only match a single line (unique combination of "command:optname")
  s4m_immco_targetopt=`echo "$s4m_immco_modopts" | grep "$s4m_immco_cmd:$s4m_immco_optname:"`

  s4m_immco_cmdrequired=`s4m_cmdopt_extract_required "$s4m_immco_targetopt"`

  s4m_debug "  Option $s4m_immco_optname is [$s4m_immco_cmdrequired]" 2
  if [ "$s4m_immco_cmdrequired" = "MANDATORY" ]; then
    return 0
  fi
  return 1
}


## Function:  s4m_get_cmd_opts_flag_value
##
## Get the value given for a command flag by providing the option name
## (long flag), short flag and a multiline list of command option tokens
## from user input.
##
## Test/s:  TODO
##
s4m_get_cmd_opts_flag_value () {
  s4m_gcofv_optname="$1"
  s4m_gcofv_optflag="$2"
  s4m_gcofv_optslist="$3"
  s4m_gcofv_retval=""

  s4m_gcofv_foundopts=`echo "$s4m_gcofv_optslist" | grep -n -P "^(-$s4m_gcofv_optflag|--$s4m_gcofv_optname)$" 2>&1`
  if [ $? -eq 0 ]; then
    ## If multiple flags matching target, take the LAST one supplied
    s4m_gcofv_optindex=`echo "$s4m_gcofv_foundopts" | cut -d':' -f 1 | tail -1`
    ## a string which doesn't start with a dash is taken to be a value
    s4m_gcofv_retval=`echo "$s4m_gcofv_optslist" | awk 'NR=='$s4m_gcofv_optindex'+1 {print $0}' | grep -v -P "^\-"`

    s4m_debug "s4m_get_cmd_opts_flag_value(): optindex=[$s4m_gcofv_optindex], retval=[$s4m_gcofv_retval]" 3

  else
    echo "$s4m_gcofv_retval"
    return 1
  fi

  echo "$s4m_gcofv_retval"
}


## TODO: T#2031: Remove when validated that we don't need this any more
##
## Function:  s4m_get_inifile_path_for_module
##
## Return module.ini file path for a module.
## If module.ini file not found for given module, return empty string.
##
## Test/s:  TODO
##
#s4m_get_inifile_path_for_module () {
#  s4m_gipfm_modulename="$1"
#  s4m_gipfm_modulepath="$2"
#  if [ ! -d "$s4m_gipfm_modulepath" ]; then
#    s4m_gipfm_modulepath="$S4M_MODULE_HOME"
#  fi
#
#  s4m_gipfm_moduleini="$s4m_gipfm_modulepath/$s4m_gipfm_modulename/module.ini"
#  if [ -f "$s4m_gipfm_moduleini" ]; then
#    echo "$s4m_gipfm_moduleini"
#    return
#  fi
#  echo ""
#}


## Function:  s4m_cmd_strip_namespace
##
## For command string <namespace>::<command>, strip "<namespace>::" component
## and return resulting string
##
## Test/s:  TODO
##
s4m_cmd_strip_namespace () {
  echo "$1" | sed -r -e 's|^[A-Za-z0-9_]+\:\:||'
}


## Function:  s4m_cmdreg_get_matches
##
## Return lines matching input command, prefixed by registry line number
## Return format:
##        <i>:<modulename>:<namespace>:<command>:<shellscript>
##        <i+1>:<modulename>:<namespace>:<command>:<shellscript>
##        ...
## Test/s:  TODO
##
## PORTABILITY: grep -P usage
##
s4m_cmdreg_get_matches () {
  s4m_cgm_search="$1"

  echo "$s4m_cgm_search" | grep "::" > /dev/null 2>&1
  ## Match on namespace and command
  if [ $? -eq 0 ]; then
    s4m_cgm_namespace=`echo "$s4m_cgm_search" | sed 's/\:\:.*$//'`
    s4m_cgm_command=`echo "$s4m_cgm_search" | sed 's/^.*\:\://'`

    s4m_debug "s4m_cmdreg_get_matches(): Got namespace=[$s4m_cgm_namespace], command=[$s4m_cgm_command]" 3

    ##: NOTE: S4M_COMMAND_REGISTRY exported earlier in 's4m' master script
    s4m_cgm_matches=`echo "$S4M_COMMAND_REGISTRY" | grep -P -n "\:$s4m_cgm_namespace\:$s4m_cgm_command\:"  2> /dev/null`

  ## Match on command only
  else
    s4m_cgm_matches=`echo "$S4M_COMMAND_REGISTRY" | cut -d':' -f 3 | grep -P -n "^$s4m_cgm_search$"  2> /dev/null`
  fi
  s4m_cgm_matchnums=`echo "$s4m_cgm_matches" | cut -d':' -f 1`

  s4m_debug "s4m_cmdreg_get_matches(): Got s4m_cgm_matchnums=[$s4m_cgm_matchnums]" 3

  s4m_cmdreg_get_lines "$s4m_cgm_matchnums"
}


## Function:  s4m_cmdreg_get_lines
##
## Return lines by registry line indexes
## Return format:
##        <modulename>:<namespace>:<command>:<shellscript>
##        ...
## Test/s:  TODO
##
s4m_cmdreg_get_lines () {
  ## space or newline separated line numbers
  s4m_cgl_linenums="$1"
  s4m_cgl_targetlines=""
  for n in $s4m_cgl_linenums
  do
    ##: NOTE: S4M_COMMAND_REGISTRY exported earlier in 's4m' master script
    s4m_cgl_line=`echo "$S4M_COMMAND_REGISTRY" | awk "NR==$n"`
    s4m_cgl_targetlines="$s4m_cgl_targetlines
$s4m_cgl_line"
  done
  echo "$s4m_cgl_targetlines" | grep "\w"
}


## Function:  s4m_get_colon_separated_token
##
## Extract target token by index from a colon-separated string.
## If bad target string format, return empty string.
##
## Test/s:  TODO
##
s4m_get_colon_separated_token () {
  s4m_gcst_targetstring="$1"
  s4m_gcst_tokenindex="$2"
  ## return token and trim trailing comma, if any
  echo "$s4m_gcst_targetstring" | grep ":" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "$s4m_gcst_targetstring" | cut -d':' -f $s4m_gcst_tokenindex | sed 's|\,$||'
    return
  fi
  echo ""
}


## Get colon-separated token from CommandOptions spec, by token index
## Test/s:  TODO
s4m_cmdopt_extract_token () {
  s4m_get_colon_separated_token "$1" "$2"
}


## Quick way to retrieve 'command' name (first token)
## Test/s:  TODO
s4m_cmdopt_extract_command () {
  s4m_cmdopt_extract_token "$1" 1
}

## Quick way to retrieve 'option name' (2nd token)
## Test/s:  TODO
s4m_cmdopt_extract_optname () {
  s4m_cmdopt_extract_token "$1" 2
}

## Quick way to retrieve 'option description' (3rd token)
## Test/s:  TODO
s4m_cmdopt_extract_desc () {
  s4m_cmdopt_extract_token "$1" 3
}

## Quick way to retrieve 'required' (boolean, 4th token)
## Test/s:  TODO
s4m_cmdopt_extract_required () {
  s4m_cmdopt_extract_token "$1" 4
}

## Quick way to retrieve 'flag' (5th and final token)
## Test/s:  TODO
s4m_cmdopt_extract_shortflag () {
  s4m_cmdopt_extract_token "$1" 5
}


## Get colon-separated token from command registry line by given index
## Test/s:  TODO
s4m_cmdreg_extract_token () {
  s4m_get_colon_separated_token "$1" "$2"
}

## Quick way to retrieve 'module' name (first token)
## Test/s:  TODO
s4m_cmdreg_extract_module () {
  s4m_cmdreg_extract_token "$1" 1
}

## Quick way to retrieve 'namespace' (second token)
## Test/s:  TODO
s4m_cmdreg_extract_namespace () {
  s4m_cmdreg_extract_token "$1" 2
}

## Quick way to retrieve 'command' (third token)
## Test/s:  TODO
s4m_cmdreg_extract_command () {
  s4m_cmdreg_extract_token "$1" 3
}

## Quick way to retrieve 'script' (fourth token)
## Test/s:  TODO
s4m_cmdreg_extract_script () {
  s4m_cmdreg_extract_token "$1" 4
}

## Get module database value for given key.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_module_db_get
##
s4m_module_db_get () {
  s4m_mdg_key="$1"
  s4m_mdg_ini=`s4m_get_module_db_file "$S4M_MODULE"`

  s4m_debug "s4m_module_db_get(): Got ini file=[$s4m_mdg_ini]" 3

  ret=`s4m_inifile_get "$s4m_mdg_ini" "$s4m_mdg_key"`
  if [ $? -ne 0 -o -z "$ret" ]; then
    return 1
  fi
  echo "$ret"
}


## Get module version from module configuration file.
## Assumes current module unless overriden with module name.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_module_version
##
## T#2561: TODO: Refactor to remove or make module path mandatory?
##
s4m_module_version () {
  s4m_mv_module="$1"
  if [ -z "$s4m_mv_module" ]; then
    s4m_mv_module="$S4M_MODULE"
  fi
  s4m_mv=`s4m_get_module_config_item "$s4m_mv_module" Version "$S4M_MODULE_PATH"`
  if [ -z "$s4m_mv" ]; then
    s4m_error "No version string found for module $s4m_mv_module!"
    return 1
  fi
  echo "$s4m_mv"
}


## Get path to module database (.ini) file.
## 
## Originally intended to support multiple .ini files based on module version,
## but that might be unnecessarily complicated.
##
## So presently just assuming a single .ini file path which will be valid as
## long as the module name doesn't change!
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_get_module_db_file
##
s4m_get_module_db_file () {
  #s4m_gmdf_module_vers=`s4m_module_version`
  #if [ $? -eq 0 ]; then
  #  echo "$S4M_MODULE_DB/$S4M_MODULE/${S4M_MODULE}_${s4m_gmdf_module_vers}.ini"
  #  return
  #fi
  echo "$S4M_MODULE_DB/$S4M_MODULE/database.ini"
}


## Test whether module database contains given key.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_module_db_has_key
##
s4m_module_db_has_key () {
  s4m_mdhk_key="$1"
  val=`s4m_module_db_get "$s4m_mdhk_key"`
  if [ $? -ne 0 -o -z "$val" ]; then
    return 1
  fi
}

