## S4M settings
##
## NOTE: Not intended for direct execution.


### GLOBALS ###

if [ -z "$S4M_SCRIPT_NAME" ]; then
  S4M_SCRIPT_NAME="s4m"
fi
if [ -z "$S4M_SCRIPT_TITLE" ]; then
  S4M_SCRIPT_TITLE="S4M - (S)hell-wrapper (for) (M)odular scripting"
fi
if [ -z "$S4M_SCRIPT_VERSION" ]; then
  S4M_SCRIPT_VERSION="0.10"
fi

## Externally modified build / bundle date (e.g. displayed in s4m version output)
## DO NOT MODIFY UNLESS YOU KNOW WHAT YOU'RE DOING
S4M_BUILD_DATE="2019-10-04"

## S4M user config folder
S4M_USER_HOME="$HOME/.s4m"

## If not overriden, set default config location now
if [ -z "$S4M_DEFAULT_CONFIG" ]; then
  S4M_DEFAULT_CONFIG="$S4M_USER_HOME/config"
fi

## Data cache
S4M_USER_CACHE="$S4M_USER_HOME/cache"

## Module user database
S4M_MODULE_DB="$S4M_USER_HOME/moduledata"

## Exit immediately on error? Default 0. Use 1 to enable
S4M_HALT_ON_ERROR=0

## A module should exit when a routed command (via "s4m_route") is finished.
## This is the default behaviour. Do not change unless you know what you're
## doing!
S4M_EXIT_ON_ROUTE=TRUE

## Global debug flag. Don't override if already set.
## Default is FALSE
if [ -z "$S4M_DEBUG" ]; then
  S4M_DEBUG="FALSE"
  export S4M_DEBUG
fi
## Debug level - higher number means more low-level stuff is printed
## Default is 1 (NOTE: Only takes effect when S4M_DEBUG is TRUE)
if [ -z "$S4M_DEBUG_LEVEL" ]; then
  S4M_DEBUG_LEVEL=1
  export S4M_DEBUG_LEVEL
fi

S4M_TMP="/tmp"


export S4M_USER_HOME
export S4M_USER_CACHE
export S4M_DEFAULT_CONFIG
export S4M_HALT_ON_ERROR
export S4M_EXIT_ON_ROUTE
export S4M_TMP


## Required core programs for 's4m' to function
## TODO: When splitting s4m core to new "core" module, this will become a module binary dependency.
S4M_BINARY_DEPENDENCIES="bc
"

for bd in `echo $S4M_BINARY_DEPENDENCIES`
do
  if ! which "$bd" > /dev/null 2>&1; then
    echo "Fatal: Required $S4M_SCRIPT_NAME binary dependency '$bd' not found in environment!" 1>&2
    echo "" 1>&2
    exit 1
  fi
done

