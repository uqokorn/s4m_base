## S4M user helper functions.
##
## NOTE: Not intended for direct execution.


## Print the module version string
##
## Unit Tests:
##   TODO
##
print_module_version () {
  s4m_pmv_vers=`s4m_get_module_version`
  if [ -z "$s4m_pmv_vers" ]; then
    s4m_pmv_vers="not found"
  fi
  echo "$S4M_MODULE module version: $s4m_pmv_vers"
}


## Map s4m command to function name within module that will process the
## command and args.
##
## Format: s4m_route <command> <module_function_name>
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_routing
##
s4m_route () {
  s4m_r_route_cmd="$1"
  s4m_r_user_func="$2"

  s4m_debug "s4m_route(): S4M_MOD_CMD=[$S4M_MOD_CMD], S4M_MOD_OPTS=[$S4M_MOD_OPTS], route_cmd=[$1], user_func=[$s4m_r_user_func]" 4

  if [ "$S4M_MOD_CMD" = "$1" ]; then
    ## validate existence of target function
    set | grep -P "^$2\s*\(\)\s*$" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      set -- "$S4M_MOD_OPTS"
      s4m_debug "s4m_route(): calling  eval \"$s4m_r_user_func $@\"" 4
      ## call target function, pass command args
      eval "$s4m_r_user_func $@" 
      ret=$?
      ## We normally should exit after routing a command to a user function.
      ## S4M_EXIT_ON_ROUTE is not something that should be turned off globally but
      ## only while testing / debugging
      if [ "$S4M_EXIT_ON_ROUTE" = "TRUE" ]; then
        if [ $ret != 0 ]; then
          s4m_log "Exiting with non-zero status ($ret)"
        fi
        exit $ret
      fi
      s4m_debug "Post-route exiting is turned off, will continue after returning from routed command '$S4M_MOD_CMD'"
      return $ret

    else
      s4m_error "Module command handler function '$s4m_r_user_func()' not found in module $S4M_MODULE source!"
      return 1
    fi
  fi
}


## Import a module script.
##
## If giving just a module name, implies we're looking for a shell script with
## same name as module e.g. module "foo" implies load "foo/foo.sh".
##
## Can provide as input "module/alternate.sh" to load specific script from
## target module only.
##
## DONE: T#2561: Update for canonical module search paths (not tackling module vers spec yet)
## TODO: T#2561: Test
##
## WARNING: This function will require update if/when introducing module co-versioning.
##
s4m_import () {
  ## Can be module name only, or "module/script.sh" or module:/nested/target.sh
  si_module_target="$1"
  ## NOTE: For future implementation of coexisting multiple module versions
  si_module_version="$2"

  si_found_path=`s4m_check_module_exists "$si_module_target" 2>/dev/null`
  ## Use function to see if input is a module name - ignore warnings/errors
  ## If zero return status, then input was just a module name (and/or module vers spec)
  if [ $? -eq 0 ]; then
    ## Just module name implies we want to import a script with the same name or
    ## there's only one script to import
    si_target="$si_found_path/${si_module_target}.sh"
    if [ -f "$si_target" ]; then
      . "$si_target"
      return $?
    fi

  ## Input was not a module, is it a path to a module script?
  else
    si_found_ret=1
    ## If we have ":" separator we take what follows as path to target relative
    ## to module top level. This works if subdirectory path has only a single
    ## level too.
    echo "$si_module_target" | grep ":" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      ## Extract modul ename
      si_mod_name=`echo "$si_module_target" | cut -d':' -f 1`
      ## remove leading slash if any
      si_script_name=`echo "$si_module_target" | cut -d':' -f 2 | sed -r -e 's|^\/||'`
      ## Does the module exist? 
      si_mod_path=`s4m_check_module_exists "$si_mod_name" 2>/dev/null`
      if [ $? -ne 0 ]; then
        s4m_error "Import target '$si_module_target' refers to a module that was not found!"
        return 1
      fi
      ## Look for the script in module path
      si_found=`find "$si_mod_path/" -type f -print0 | grep -FzZ "$si_script_name" 2>/dev/null`
      si_found_ret=$?

    ## *** DEPRECATED - BACKWARDS COMPATIBILITY ONLY ***
    ## The case where we have "module/script.sh". For modules written for S4M v0.6 or earlier.
    ## TODO: We can remove this block once all modules are using format "module:/path/to/script.sh"
    ##   in their import statements!
    else
      echo "$si_module_target" | grep "\/" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        si_mod_name=`dirname "$si_module_target"`
        si_script_name=`basename "$si_module_target"`
        si_target_path=""
        ## Does the module exist? 
        ### DEBUG: Do this first so we can see any errors - and then do it again and throw away STDERR ###
        #s4m_check_module_exists "$si_mod_name" >/dev/null
        ### END ###
        si_mod_path=`s4m_check_module_exists "$si_mod_name" 2>/dev/null`
        if [ $? -ne 0 ]; then
          s4m_error "Import target '$si_module_target' refers to a module ($si_mod_path) that was not found!"
          return 1
        fi
        si_found=`find "$si_mod_path/" -type f -print0 | grep -FzZ "$si_script_name" 2> /dev/null` 
        si_found_ret=$?
      fi
    fi

    if [ $si_found_ret -eq 0 ]; then
      si_num_found=`echo "$si_found" | wc -l`
      ## More than one match found, use one with highest module version
      if [ $si_num_found -gt 1 ]; then
        si_target_path=`echo "$si_found" | sort -n | tail -1`
      else
        si_target_path="$si_found"
      fi
    fi
    if [ ! -z "$si_target_path" ]; then
      . "$si_target_path"
      return $?
    else
      s4m_error "Import target '$si_module_target' not found!"
      return 1
    fi
  fi
}


## Run s4m from within a module or elsewhere (if name or path changes,
## this function will always work) and is hence the recommended way to
## invoke s4m manually if needed.
##
s4m_exec () {
  if [ -x "$S4M_SCRIPT_HOME"/$S4M_SCRIPT_NAME ]; then
    S4M_EXEC="TRUE"; export S4M_EXEC
    "$S4M_SCRIPT_HOME"/$S4M_SCRIPT_NAME $@
    return $?
  else
    s4m_fatal_error "[SUF1] Could not determine path to '$S4M_SCRIPT_NAME'!"
  fi
}

