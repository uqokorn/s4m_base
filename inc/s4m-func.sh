## S4M shell utility functions
##
## NOTE: Not intended for direct execution.

S4M_TS_FORMAT="%Y%m%d_%H%M%S"; export S4M_TS_FORMAT


### FUNCTIONS ###

## Replace echo with a version that will work as expected with no nasty
## side effects from special characters in input string.
##
## *** NOTE: For "echo -n" must call /bin/echo -n explicitly! ***
##
echo () {
  printf %s\\n "$*" ;
}


## Test if a token belongs in a shell array (space or newline separated),
## or comma-separated string where tokens are separated by commas.
##
## @PORTABILITY : grep "-P" only works in Linux
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_in_array*
##
s4m_in_array () {
  needle="$1"
  shift
  haystack=$@
  echo "$needle" | grep "," > /dev/null 2>&1
  needle_has_commas=$?
  echo "$haystack" | grep "," > /dev/null 2>&1
  haystack_has_commas=$?
  ## needle can only contain commas if haystack does not
  if [ $needle_has_commas -eq 0 -a $haystack_has_commas -eq 0 ]; then
    return 1
  fi
  echo "$haystack" | grep -P "^$needle\b|\b$needle\b" > /dev/null 2>&1
  return $?
}


## Return timestamp formatted according to 'S4M_TS_FORMAT'
s4m_timestamp () {
  date +"$S4M_TS_FORMAT"
}


## Return the corresponding long module arg name for a given short arg.
## If input is not a recognised short arg for given module (e.g. long flag
## was given, or short flag not existing) then returned unchanged.
##
## NOTE: Saving error messages because we need to keep STDOUT and STDERR
##       clean (caller captures STDOUT)
##
## SECURITY: Returned string is used in an 'eval' to set a variable dynamically.
##       We validate user string before returning.
##
s4m_short_arg_to_long_arg () {
  s4m_satl_arg="$1"

  if [ -z "$s4m_satl_arg" ]; then
    s4m_push_error "Internal: Attempt to resolve empty module arg name to long arg name."
    return 1
  fi
 
  ## Get command options (flag) lines from module's .ini file 
  ## We don't want the module name so cut that out so we can use
  ## the "s4m_cmdopt_extract_<fieldname>" helper functions.
  s4m_satl_mod_opts=`s4m_return_one_module_command_options "$S4M_MODULE" "$S4M_MODULE_PATH" | cut -d':' -f 2-`
  if [ $? -ne 0 ]; then
    s4m_push_error "Internal: Failed to retrieve command options for module '$S4M_MODULE'"
    return 1
  fi

  s4m_satl_retval=""

  ## If found short flag match in opts list, return long arg name
  while read optline
  do
    ## find opts line for current command - flags can appear more than once!
    ## (but only once per command)
    echo "$optline" | grep -P "^\s*$S4M_MOD_CMD\b" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      s4m_satl_long=`s4m_cmdopt_extract_optname "$optline"`
      s4m_satl_short=`s4m_cmdopt_extract_shortflag "$optline"`
      ## input matches short arg, return corresponding long arg
      if [ "$s4m_satl_short"  = "$s4m_satl_arg" ]; then
        s4m_debug "  s4m_short_arg_to_long_arg(): Resolved long opt name [$s4m_satl_long] from matched short flag [$s4m_satl_short]" 4
        s4m_satl_retval="$s4m_satl_long"
      ## input matches long arg, return unchanged
      elif [ "$s4m_satl_long" = "$s4m_satl_arg" ]; then
        s4m_debug "  s4m_short_arg_to_long_arg(): Returning input '$s4m_satl_long' unchanged" 4
        s4m_satl_retval="$s4m_satl_long"
      fi
    fi
  done <<EOF
$s4m_satl_mod_opts
EOF

  if [ ! -z "$s4m_satl_retval" ]; then
    echo "$s4m_satl_retval"
    return 0
  fi

  s4m_push_warn "Skipping capture of unknown flag '$s4m_satl_arg'"
  return 1
}


## Parse module args into automatic variables named like: "<module>_<longflag>"
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_module_args_capture
##
s4m_capture_module_args () {

  s4m_cma_optslist=`echo "$@" | tr " " "\n"` 

  s4m_cma_lastflag=""

  for arg in $s4m_cma_optslist
  do
    echo "$arg" | grep -P "^\-\w+|\-\-\w+" > /dev/null 2>&1
    ## arg is a flag
    if [ $? -eq 0 ]; then
      #s4m_debug "Capturing flag '$arg'"
      ## strip dashes
      arg=`echo "$arg" | sed -r -e 's|^[\-]+||'`

      ## get long arg name if given as short arg
      s4m_cma_argname=`s4m_short_arg_to_long_arg "$arg"`
      ret=$?

      if [ $ret -ne 0 ]; then
        s4m_pop_warn
        if s4m_pop_error; then
          return 1
        fi
      else
        s4m_debug "Validated flag '$arg' ($s4m_cma_argname)"
      fi
      ## only do something if we have a valid flag
      if [ ! -z "$s4m_cma_argname" ]; then
        ## two flags in a row? set previous to implicit "true" value
        ## (automatic value for a 'toggle' flag)
        if [ ! -z "$s4m_cma_lastflag" ]; then
          s4m_debug "Setting variable '${S4M_MODULE}_${s4m_cma_lastflag}' = 'true'"
          eval "${S4M_MODULE}_${s4m_cma_lastflag}=\"true\"; export ${S4M_MODULE}_${s4m_cma_lastflag}"
        fi
        s4m_cma_lastflag="$s4m_cma_argname"
      else
        s4m_debug "Flag $arg was not validated against module.ini"
      fi

    ## arg is a value
    else
      ## Remove backslashes if there's any remaining after interpolation
      arg=`printf "%b" "$arg"`
      ## Removes backslashes - the extra slash is for the extra eval layer.
      ## NOTE: If we use "tr -d '\'" we get a warning about non-portable backlash at end of string,
      ## so we use "tr -d '\\'" instead.
      arg=`echo "$arg" | tr -d '\\\'`

      ## this value belongs to the previous flag - assign it.
      ## any 'hanging' values are ignored.
      if [ ! -z "$s4m_cma_lastflag" ]; then
        ## create new variable called "<module>_<longflag>"
        varname="${S4M_MODULE}_${s4m_cma_lastflag}"
        ## $<module>_<flag> already exists, append new value
        if test -n "`eval echo \"\\$\$varname\"`"; then
          s4m_debug "Flag var '$varname' already captured (this is a duplicate), appending new value '$arg'"
          oldval=`eval echo \"\\$\$varname\"`
          eval "$varname=\"$oldval
$arg\"; export $varname"
          newval=`eval echo "\\$\$varname"`
          s4m_debug "Var '$varname' = [$newval]"
        else
          s4m_debug "Setting variable '$varname' = $arg"
          eval "$varname=\"$arg\"; export $varname"
        fi
        s4m_cma_lastflag=""
      fi
    fi
  done

  ## If final flag was a toggle the give it "true" value
  if [ ! -z "$s4m_cma_lastflag" ]; then
    varname="${S4M_MODULE}_${s4m_cma_lastflag}"
    s4m_debug "Setting variable '$varname' = 'true'"
    eval "$varname=\"true\"; export $varname"
  fi
}


## Print a debug message if global debugging enabled.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_debug_toggle_value
##   modules/_selftest/_selftest.sh:test_debug_toggle_operation
s4m_debug () {
  if [ "$S4M_DEBUG" = "TRUE" ]; then
    s4m_d_ts=`s4m_timestamp`
    s4m_d_mod=""
    if [ ! -z "$S4M_MODULE" ]; then
      s4m_d_mod=" $S4M_MODULE"
    fi
    s4m_d_msg_level="$2"
    ## If not given, we assume the target debug level is 1 so that by default,
    ## all s4m_debug() calls without level restrictions are going to print when
    ## global debugging is turned on
    if [ -z "$s4m_d_msg_level" ]; then
      s4m_d_msg_level=1
    ## Got message level restriction, check is a valid number
    else
      echo "$s4m_d_msg_level" | grep -P "^[0-9]+$" > /dev/null 2>&1
      if [ $? -ne 0 ]; then
        ## NaN - ignore error printing and return error status
        return 1
      fi
    fi
    ## Print debug message IFF target debug level >= message debug level
    if [ $S4M_DEBUG_LEVEL -ge $s4m_d_msg_level ]; then
      echo "[${s4m_d_ts}${s4m_d_mod}] DEBUG: $1" 1>&2
    fi
  fi
}

## Test whether we're in debug mode
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_debug_toggle_operation
s4m_isdebug () {
  if [ "$S4M_DEBUG" = "TRUE" ]; then
    return 0
  fi
  return 1
}

## Print a log message
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_log
##   
s4m_log () {
  s4m_l_ts=`s4m_timestamp`
  s4m_l_mod=""
  if [ ! -z "$S4M_MODULE" ]; then
    s4m_l_mod=" $S4M_MODULE"
  fi
  echo "[${s4m_l_ts}${s4m_l_mod}] $1"
}


## Print an error message and halt execution.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_fatal_error_exit_status
##
s4m_fatal_error () {
  s4m_fe_ts=`s4m_timestamp`
  s4m_fe_mod=""
  if [ ! -z "$S4M_MODULE" ]; then
    s4m_fe_mod=" $S4M_MODULE"
  fi
  s4m_fe_errcode=1
  if [ ! -z "$2" ]; then
    s4m_fe_errcode="$2"
  fi
  echo "
[${s4m_fe_ts}${s4m_fe_mod}] ERROR: $1
" 1>&2
  eval "exit $s4m_fe_errcode"
}


## Print an error message.
## If global override S4M_HALT_ON_ERROR is set, redirect to fatal error.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_error
##
s4m_error () {
  s4m_e_ts=`s4m_timestamp`
  s4m_e_mod=""
  if [ ! -z "$S4M_MODULE" ]; then
    s4m_e_mod=" $S4M_MODULE"
  fi
  if [ ! -z "$S4M_HALT_ON_ERROR" -a "$S4M_HALT_ON_ERROR" -eq 1 ]; then
    s4m_fatal_error "$1" "$2"
  fi
  echo "
[${s4m_e_ts}${s4m_e_mod}] ERROR: $1
" 1>&2
}


## Print a warning message to STDERR device.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_warn
##
s4m_warn () {
  s4m_w_ts=`s4m_timestamp`
  s4m_w_mod=""
  if [ ! -z "$S4M_MODULE" ]; then
    s4m_w_mod=" $S4M_MODULE"
  fi
  echo "
[${s4m_w_ts}${s4m_w_mod}] WARN: $1
" 1>&2
}


## Save last error message for future retrieval
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_push_messages
##
s4m_push_error () {
  S4M_LAST_ERROR=`s4m_error "$1" 2>&1`
  export S4M_LAST_ERROR
}


## Save last warning message for future retrieval
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_push_messages
##
s4m_push_warn () {
  S4M_LAST_WARN=`s4m_warn "$1" 2>&1`
  export S4M_LAST_WARN
}


## Retrieve last error message and clear last
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_push_messages
##
s4m_pop_error () {
  if [ ! -z "$S4M_LAST_ERROR" ]; then
    echo "$S4M_LAST_ERROR" 1>&2
    S4M_LAST_ERROR=""; export S4M_LAST_ERROR
    return
  fi
  return 1
}


## Retrieve last warning message and clear last
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_push_messages
##
s4m_pop_warn () {
  if [ ! -z "$S4M_LAST_WARN" ]; then
    echo "$S4M_LAST_WARN" 1>&2
    S4M_LAST_WARN=""; export S4M_LAST_WARN
    return
  fi
  return 1
}


## Test whether an .ini file contains a given variable.
##
## Assumptions: The target .ini file is known to exist
##
## Unit Tests: 
##   modules/_selftest/_selftest.sh:test_inifile_contains
##
s4m_inifile_contains () {
  s4m_ic_inifile="$1"
  s4m_ic_var="$2"

  if [ ! -f "$s4m_ic_inifile" ]; then
    s4m_error "INI file '$s4m_ic_inifile' not found!"
    return 1
  fi

  grep "${s4m_ic_var}=" "$s4m_ic_inifile" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "TRUE"  | tr -d [:cntrl:]
    return
  fi

  echo "FALSE" | tr -d [:cntrl:]
  return 1
}


## Get multi (or single) line value for given file and variable.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_inifile_get_multiline
##
## @PORTABILITY : grep "-P" flag is Linux only
##
s4m_inifile_get_multiline () {
  s4m_igm_inifile="$1"
  s4m_igm_var="$2"

  if [ ! -f "$s4m_igm_inifile" ]; then
    s4m_error "INI file '$s4m_igm_inifile' not found!"
    return 1
  fi

  hasvar=`s4m_inifile_contains "$s4m_igm_inifile" "$s4m_igm_var"`
  if [ $hasvar = "FALSE" ]; then
    return 1
  fi

  ## This next chunk of code merely returns a block of lines belonging to the
  ## config item in question (values spanning multiple lines).
  ##
  ## Should really use something like multiline 'sed' here although I'm time poor
  ## and the intricacies of such a solution are currently beyond me - we have to
  ## read from the target config var line to either EOF or the next config var,
  ## and trim trailing "\nfoo=bar" in the latter case.
  ##
  ## 'awk' would also work I guess - probably easier than learning the required
  ## 'sed' voodoo.

  s4m_igm_optlines=`grep -n -P "^[A-Za-z0-9_]+\=" "$s4m_igm_inifile"`
  s4m_igm_optlineindexes=`echo "$s4m_igm_optlines" | cut -d':' -f 1`
  s4m_igm_targetlineindex=`echo "$s4m_igm_optlines" | grep -P "^[0-9]+:${s4m_igm_var}" | cut -d':' -f 1`
  ## work out what the next option line is after our target
  s4m_igm_nextopt=`echo "$s4m_igm_optlineindexes" | grep -A 1 -P "^${s4m_igm_targetlineindex}\$" | tail -1`
  if [ $s4m_igm_nextopt -gt $s4m_igm_targetlineindex ]; then
    s4m_igm_lines=`expr $s4m_igm_nextopt - $s4m_igm_targetlineindex - 1`
  else
    s4m_igm_eofline=`cat "$s4m_igm_inifile" | wc -l`
    s4m_igm_lines=`expr $s4m_igm_eofline - $s4m_igm_targetlineindex`
  fi
  ## Use grep's handy "after" context with pre-determined N-lines to print only
  ## the lines we need, excluding comment lines
  s4m_igm_val=`grep -P "^${s4m_igm_var}" -A $s4m_igm_lines "$s4m_igm_inifile" | grep -v -P "^#" | sed "s/^${s4m_igm_var}\=//"`
  echo "$s4m_igm_val"
}


## Retrieve the value of a given SINGLE LINE variable from an .ini file.
##
## Unit Tests: 
##   modules/_selftest/_selftest.sh:test_inifile_get
##
s4m_inifile_get () {
  s4m_ig_inifile="$1"
  s4m_ig_var="$2"

  if [ ! -f "$s4m_ig_inifile" ]; then
    s4m_error "INI file '$s4m_ig_inifile' not found!"
    return 1
  fi

  s4m_ig_matchline=`grep "^${s4m_ig_var}=" "$s4m_ig_inifile"`
  s4m_ig_nmatch=`echo "$s4m_ig_matchline" | wc -l`
  if [ $s4m_ig_nmatch -gt 1 ]; then
    s4m_error "Ambiguous config variable '$s4m_ig_var' returned > 1 result in target '$s4m_ig_inifile'"
    return 1

  else
    echo "$s4m_ig_matchline" | sed 's/^[A-Za-z0-9_]\+=//'
  fi
}


## Get the path to current shell executable
##
## Unit Tests:
##   modules/_sefltest/_selftest.sh:test_shell_discovery
s4m_get_shell () {
  ## In order of preference..
  s4m_gs_find_shells="bash sh"
  if [ ! -z "$SHELL" ]; then
    echo "$SHELL"
  else
    for s in $s4m_gs_find_shells; do
      s4m_gs_matches=`type "$s"`
      if [ $? -eq 0 ]; then
        s4m_gs_shell=`echo "$s4m_gs_matches" | tail -1 | tr " " "\n" | tac | head -1`
        readlink -f "$s4m_gs_shell"
        return
      fi
    done
  fi
}


## Check for non-coreutils, non-POSIX dependencies
## TODO: Complete or remove if not used
s4m_check_dependencies () {
  noncore_deps="grep sed"
}


## Set new value for an .ini file
##
## NOTE: Cannot set multi-line values!
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_set_config_item
##
s4m_ini_write_val () {
  s4m_iwv_inifile="$1"
  s4m_iwv_var="$2"
  s4m_iwv_val="$3"
  if [ ! -f "$s4m_iwv_inifile" ]; then
    s4m_fatal_error "[SF1] Target config file '$s4m_iwv_inifile' not found or not writeable"
  else
    varexists=`s4m_inifile_contains "$s4m_iwv_inifile" "$s4m_iwv_var"`
    if [ "$varexists" = "TRUE" ]; then
      sed -i -r "s|^$s4m_iwv_var\=.*$|$s4m_iwv_var\=$s4m_iwv_val|" "$s4m_iwv_inifile"
    else
      s4m_fatal_error "[SF2] Target config file '$s4m_iwv_inifile' has no variable named '$s4m_iwv_var'"
    fi
  fi
}


## Set value for a variable in S4M config file
##
## NOTE: Cannot set multi-line values!
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_set_config_item
##
s4m_set_config_item () {
  s4m_sci_var="$1"
  s4m_sci_val="$2"

  s4m_sci_varexists=`s4m_inifile_contains "$S4M_DEFAULT_CONFIG" "$s4m_sci_var"`
  if [ ! -z "$s4m_sci_var" -a -f "$S4M_DEFAULT_CONFIG" -a $s4m_sci_varexists = "TRUE" ]; then
    s4m_ini_write_val "$S4M_DEFAULT_CONFIG" "$s4m_sci_var" "$s4m_sci_val"
    if [ $? -eq 0 ]; then
      return
    fi
    s4m_fatal_error "[SF3] Failed to write S4M config item '$s4m_sci_var'!"
  ## append var=val to config file
  elif [ -f "$S4M_DEFAULT_CONFIG" ]; then
    echo "$s4m_sci_var=$s4m_sci_val" >> "$S4M_DEFAULT_CONFIG"
  else
    s4m_fatal_error "[SF4] Cannot write to missing config file '$S4M_DEFAULT_CONFIG'!"
  fi
}


## Fetch value for a config item.
##
## NOTE: This should never be called without 's4m_check_load_config' being called first.
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_get_config_item
## 
s4m_get_config_item () {
  s4m_gci_var="$1"
  if [ ! -z "$S4M_DEFAULT_CONFIG" ]; then
    echo `s4m_inifile_get "$S4M_DEFAULT_CONFIG" "$s4m_gci_var"`
    return
  fi
  echo ""
}


## Convert relative paths to absolute paths.
##
## Especially handy to convert user paths because modules execute out of their own
## module subdirectory - it's nice to be able to convert user relative paths
## from point of s4m execution into absolute paths to pass into modules so we
## can completely remove that responsibility from modules.
##
## @PORTABILITY : grep "-P" flag is Linux only
## @PORTABILITY : readlink "-f" appears to be Linux only
##
## Unit Tests:
##   modules/_selftest/_selftest.sh:test_s4m_rel_to_abs_path
##
s4m_rel_to_abs_path () {
  s4m_rtap_path="$1"
  ## Path was given as relative "./foo" or "../foo", OR a file or directory relative to user's CWD
  ##
  echo "$s4m_rtap_path" | grep -P "^\.+\/*" > /dev/null 2>&1
  if [ $? -eq 0 -o -f "$s4m_rtap_path" -o -d "$s4m_rtap_path" ]; then
    s4m_rtap_fixed=`readlink -f "$s4m_rtap_path" 2>&1`
    echo "$s4m_rtap_fixed"
    return
  else
    ## if we fell through, return the input as it was given
    echo "$s4m_rtap_path"
  fi
}


## Test Internet connectivity
##
s4m_has_internet_connection () {
  s4m_test_host="google.com"
  s4m_test_uri="http://$s4m_test_host"

  which wget > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    ## Check access to URI without downloading it
    wget -q --timeout=10 --spider "$s4m_test_uri"
    return $?
  else
    which nc > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      s4m_push_error "No 'wget' or 'nc', cannot determine Internet connection status!"
      return 1
    fi
    echo -e "GET $s4m_test_uri HTTP/1.0\n\n" | nc "$s4m_test_host" 80 > /dev/null 2>&1
    return $?
  fi
}

