#!/bin/sh

## Get module help information
##
## Unit tests: TODO
##
core_get_module_help_info () {
   ## Can be module name only, or with version "module/version"
   cgmhi_module="$1"
   cgmhi_modulepath="$2"
   cgmhi_slavemode="$3"

   cgmhi_less="less"
   if [ ! -z "$cgmhi_slavemode" ]; then
     cgmhi_less="cat -"
   fi

   cgmhi_vers=`s4m_get_module_config_item "$cgmhi_module" "Version" "$cgmhi_modulepath"`
   cgmhi_author=`s4m_get_module_config_item "$cgmhi_module" "Author" "$cgmhi_modulepath"`
   cgmhi_desc=`s4m_get_module_config_item "$cgmhi_module" "Description" "$cgmhi_modulepath"`
   cgmhi_usage=`s4m_get_module_usage "$cgmhi_module" "$cgmhi_modulepath" 2>&1`
   cgmhi_ns=`s4m_get_module_config_item "$cgmhi_module" "Namespace" "$cgmhi_modulepath"`

   cgmhi_module_name="$cgmhi_module"
   echo "$cgmhi_module" | grep "\/" > /dev/null 2>&1
   if [ $? -eq 0 ]; then
     cgmhi_module_name=`echo "$cgmhi_module" | cut -d'/' -f 1`
   fi

   cgmhi_hint=""
   if [ -z "$cgmhi_slavemode" ]; then
     cgmhi_hint="(CTRL-C or 'q' to exit)"
     clear
   fi
   /bin/echo -n "  S4M HELP ($cgmhi_module)
  =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  Module: $cgmhi_module_name v$cgmhi_vers, by $cgmhi_author

  Description: $cgmhi_desc

  Usage: $S4M_SCRIPT_NAME [$cgmhi_ns::]<command> <flags>

$cgmhi_usage

  =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  $cgmhi_hint
" | $cgmhi_less
}

