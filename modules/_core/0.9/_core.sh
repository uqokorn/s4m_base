#/bin/sh
# Module: _core
# Author: Othmar Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

. ./inc/core_functions.sh


## Route the help command appropriately
## e.g. following user command:
##
##   s4m help foo
##
## If "foo" is a module, return its help documentation
## (module description and all of its command help)
##
## If "foo" is a command, return help documentation for that command only, or
## request for clarification (e.g. bar::foo, foo::foo)
##
core_help_handler () {
 
  s4m_debug "Got command = [$S4M_MOD_CMD]" 3
  s4m_debug "Got args for help command = [$S4M_MOD_OPTS]" 3

  ## No arguments to "help" command - invoke built-in help
  if [ -z "$S4M_MOD_OPTS" ]; then
    s4m_exec --help
    return $?
  fi

  echo "$S4M_MOD_OPTS" | grep " " > /dev/null 2>&1
  ## No spaces in string, it could be a module
  if [ $? -ne 0 ]; then
    ## Only show if interactive mode (not when command is "helpslave" for instance)
    if [ "$S4M_MOD_CMD" = "help" ]; then
      echo " ( searching ..) "
    fi
    possible_module="$S4M_MOD_OPTS"
    chh_modulepath=`s4m_check_module_exists "$possible_module" 2>/dev/null`
    chh_modulepath=`dirname "$chh_modulepath"`
    s4m_debug "core_help_handler(): Got chh_modulepath=[$chh_modulepath]" 4

    ## suppress any error messages here since we want nice output in case of bad user query
    if [ $? -eq 0 ]; then
      ## print module information
      if [ "$S4M_MOD_CMD" = "helpslave" ]; then
        core_get_module_help_info "$possible_module" "$chh_modulepath" slavemode
      else
        core_get_module_help_info "$possible_module" "$chh_modulepath" 
      fi
      return
    fi
  fi

  ## Not a module or module command, search term/s not found
  s4m_log "Sorry, No help information found for query '$S4M_MOD_OPTS'"

  ## Not found
  return 1
}


## Handler for 'addmodule' command
##
core_addmodule_handler () {

  s4m_debug "addmodule_handler(): Got name=[$_core_name], version=[$_core_version]" 4

  ## Default module version
  if [ -z "$_core_version" ]; then
    _core_version=0.1
  fi

  ## Create module
  ## TODO: Refactor use of 'S4M_MODULE_HOME' when multiple paths are supported!
  ##   User will probably have to specify which path to create the module in if they
  ##   do not give the full path.
  `s4m_get_shell` "$S4M_THIS_MODULE/scripts/addmodule.sh" "$S4M_MODULE_HOME" "$_core_name" "$_core_version"
  ret=$?

  if [ $ret -eq 0 ]; then
    s4m_log "Created new module '$_core_name' in path '$S4M_MODULE_HOME/$_core_name'"
  else
    s4m_error "Failed to create new module"
  fi

  return $ret
}


## Handler for 'validate-ini' command
##
core_validate_ini_handler () {

  `s4m_get_shell` ./scripts/validate_module_ini.sh "$_core_inifile"
  return $?
}


## Handler for 'install' command
## Does some args re-processing for easier extra flags to 'install' command.
core_install_module_handler () {
  s4m_debug "core_install_module_handler(): S4M_MOD_OPTS=[$S4M_MOD_OPTS]" 4
  ## Reset arg positions based on space-separated command opts string
  set -- "$S4M_MOD_OPTS"
  core_install_module_handler_actual $@
}


## Handler for 'install' command
## Calls a target module's installation script
##
core_install_module_handler_actual () {
  ## module name/version format
  cimha_module_spec="$1"

  ## No arguments to command - invoke help for this module
  if [ -z "$cimha_module_spec" ]; then
    s4m_exec helpslave "$S4M_MODULE"
    return $?
  fi

  if ! s4m_is_module_version_spec "$cimha_module_spec"; then
    s4m_error "Target '$cimha_module_spec' is not a valid module specification!
  (Must supply <module/version> argument)"
    return 1
  fi

  shift
  cimha_extraflags=""

  while [ ! -z "$1" ]
  do
    case "$1" in
      -f|--force)
        shift
        cimha_extraflags="$cimha_extraflags --force"
      ;;
      ## unrecognised, ignore
      *)
        shift
    esac
  done

  ## If target module is this module (_core) then abort
  m=`s4m_extract_module "$cimha_module_spec"`
  if [ "$m" = "$S4M_MODULE" ]; then
    s4m_error "Cannot install self (module '$S4M_MODULE')!"
    return 1
  fi 

  ## suppress any error messages here since we want nice output in case of bad user query
  cimha_modulepath=`s4m_check_module_exists "$cimha_module_spec" 2>/dev/null`
  ret=$?

  if [ $ret -eq 0 ]; then
    ## module is already in modules location; so we run its install script
    ## to check/reconfigure the module
    if [ -f "$cimha_modulepath/install.sh" ]; then
      s4m_log "Calling install script for module '$cimha_module_spec' .."
      _CORE_OLDPWD=`pwd`
      ## change into target module directory to execute install script
      cd "$cimha_modulepath" && `s4m_get_shell` "./install.sh" "$cimha_module_spec" $cimha_extraflags
      ret=$?
      cd "$_CORE_OLDPWD"
      return $ret
    else
      if [ -z "$_core_skip" ]; then
        s4m_error "Module install script '$cimha_modulepath/install.sh' not found!"
        return 1
      else
        s4m_log "No '$cimha_modulepath/install.sh' found, skipping module installation"
        return 0
      fi
    fi

  else
    s4m_error "Target module '$cimha_module_spec' not found in module path/s!"
    return 1
  fi
}


main () {

  s4m_route addmodule    core_addmodule_handler
  ## s4m install <module_spec>
  s4m_route install      core_install_module_handler
  s4m_route help         core_help_handler
  s4m_route helpslave    core_help_handler
  s4m_route validate-ini core_validate_ini_handler

}

### START ###
main

