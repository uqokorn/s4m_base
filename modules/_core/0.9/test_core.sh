#!/bin/sh
## test_core: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  . ./inc/core_functions.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests
setUp () {
  #rm -rf "$S4M_TMP/$$/"*.tab
  #touch "$S4M_TMP/$$/foo.00.c.tab"
  echo
}



## TESTS ##


## TODO: Complete - check the function that checks for valid module names
##
testAddModuleNameValidity () {
  assertNotNull "TODO"
}


## Test that we can return help information for a specific version of a module.
##     e.g. s4m help <module/version>
##
## Also tests slave mode (non-interactive) for help (i.e. the underlying function
## for "helpslave" command).
##
testMultiVersionModuleHelp () {
  mkdir "$S4M_TMP/$$/modules"
  tmp_modpath="$S4M_TMP/$$/modules"
  cp -RL "$S4M_MODULE_HOME/_core" "$tmp_modpath/"
  OLD_S4M_MODULE_HOME="$S4M_MODULE_HOME"
  ## Temporarily override module home with temp path
  S4M_MODULE_HOME="$tmp_modpath"; export S4M_MODULE_HOME

  core_mod_ver=`ls -1 "$tmp_modpath/_core/" | grep -P "^[0-9]+\.*[0-9]*$" 2> /dev/null`
  assertNotNull "$core_mod_ver"

  if [ ! -z "$core_mod_ver" ]; then
    cp -R "$tmp_modpath/_core/$core_mod_ver" "$tmp_modpath/_core/0.666"
    sed -i -r -e 's|^Version\=.*$|Version=0.666|' "$tmp_modpath/_core/0.666/module.ini"
    sed -i -r -e 's|^Description\=.*$|Description=FooBar|' "$tmp_modpath/_core/0.666/module.ini"

    ## The underlying function for an "s4m help <module/version>" command
    ret=`core_get_module_help_info "_core/0.666" "$tmp_modpath" slave`
    echo "$ret" | grep FooBar > /dev/null 2>&1
    s4m_debug "Help info for '_core/0.666': [$ret]"
    assertTrue "ret=[$ret]" $?
  fi

  S4M_MODULE_HOME="$OLD_S4M_MODULE_HOME"; export S4M_MODULE_HOME
}

