#!/bin/sh

for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## For grep -P
_CORE_MOD_INI_MANDATORY_REGEX="^Name\=
^Namespace\=
^Version\=
^Author\=
^Enabled\=
^Description\=
^VersionHistory\=
^CommandRoutes\=
^CommandOptions\=
^CommandDepends\=
^ModuleDepends\="

## Human readable
_CORE_MOD_INI_MANDATORY_FIELDS="Name
Namespace
Version
Author
Enabled
Description
VersionHistory
CommandRoutes
CommandOptions
CommandDepends
ModuleDepends"


_core_validate_module_ini () {
  ## Absolute path to target module.ini
  cvmi_inifile="$1"
  
  if [ ! -f "$cvmi_inifile" ]; then
    s4m_error "Module config '$cvmi_inifile' does not exist!"
    exit 1
  fi

  s4m_log "Validating '$cvmi_inifile'.."
  cvmi_mandatory_regex="$S4M_TMP/$$.regex"
  /bin/echo -n "$_CORE_MOD_INI_MANDATORY_REGEX" > "$cvmi_mandatory_regex"

  cvmi_results=`grep -f "$cvmi_mandatory_regex" "$cvmi_inifile" 2>/dev/null`
  if [ $? -ne 0 -o -z "$cvmi_results" ]; then
    rm -f "$cvmi_mandatory_regex"
    echo "[ FAIL ]"
    s4m_error "Target module.ini is missing all mandatory fields or a related error has occurred"
    exit 1
  fi
  ## Number of matching lines
  cvmi_num=`/bin/echo -n "$cvmi_results" | wc -l`
  ## Number of mandatory fields
  cvmi_mandatory_num=`cat "$cvmi_mandatory_regex" | wc -l`
  if [ $cvmi_num -lt $cvmi_mandatory_num ]; then
    rm -f "$cvmi_mandatory_regex"
    s4m_log "[ FAIL ]"
    s4m_error "Target module.ini is missing one or more required fields from this list:

$_CORE_MOD_INI_MANDATORY_FIELDS"
    exit 1
  fi
  rm -f "$cvmi_mandatory_regex"
  s4m_log "[ OK ]"
  s4m_log "Target module.ini passed validation."
}


### MAIN ###

## Run only if called directly (not imported)
if [ `basename $0` = "validate_module_ini.sh" ]; then
  _core_validate_module_ini "$1"
fi

