#!/bin/sh

for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done


ini_file_template () {
  modulename="$1"
  modulevers="$2"
  echo "# Module name (no spaces, alphanumeric + underscore only)
Name=$modulename
# Module command namespace. Convention: Short single word related to module name, all lower case
Namespace=$modulename
# Module version string
Version=$modulevers
# Module author
Author=A.Person
# Module contributors
Contributors=
# Module is enabled (1 or 0)
Enabled=1
# Module description (one line, no wrapping)
Description=My Module which does foo and bar
# Optional version history. Format: <version>:<date>:<changes>
# Can wrap over multiple lines
VersionHistory=$modulevers:yyyymmdd:Initial version
# Comma separated list of <command>:<script_file_name>
CommandRoutes=foo:foo.sh,bar:bar.sh
# Comma separated list of <command>:[<opt_label>]:<opt_description>:<OPTIONAL|MANDATORY>:[<opt_flag>],...
# If a command specified in CommandRoutes (above) has no flags, it must be specified here as follows:
# 	<command>::<desc>:NOFLAGS:
# For example:
#	help::Access help information:NOFLAGS:
# Can wrap over multiple lines.
CommandOptions=
# Comma separated binary dependencies in format <command>:<binary_or_command_name_or_path>,...
# Can wrap over multiple lines.
CommandDepends=
# Module dependencies, comma separated. Format:  <module_name>[==|>=<module_version>],...
# The module version and qualifier parts are optional.
# Can wrap over multiple lines.
# Example: pipeline>=0.1,foo>=2.1,bar
# Example: pipeline,foo,bar>=3.3
ModuleDepends=
"
}


user_script_template () {
  modulename="$1"
  echo "#!/bin/sh
# Module: $modulename
# Author: A.Person

# Include S4M functions (mandatory unless you're doing a \"Hello world\" module
# and don't need any API functions)
for i in \`ls \$S4M_SCRIPT_HOME/inc/*.sh\`; do . \$i; done

## ============================================================================

### Your code here ###
main () {

  ## Import other module functions etc  
  #s4m_import "othermodule/script.sh"

  ## Route command(s) from module.ini to custom shell functions defined in this
  ## shell script or included by this shell script.
  ##
  ## Route order does not matter - either one will execute or none will.
  ##
  ## This script will auto-exit immediately after a route is executed because
  ## you can only run a single command at a time.
  ##
  ## This is the recommended way to map commands to shell wrapper functions
  ## to do your work.
  ##
  s4m_route "mycommand" "my_function"
  s4m_route "anothercommand" "another_function"

  ## .. OR remove the above lines and implement your own logic flow
  ## based on the command name in variable  \$S4M_MOD_CMD

  ## Options (flags) specified in module.ini are captured and available to you
  ## by accessing variables named like  \$<module_name>_<longflagname>
  ##
  ## You can also access your full shell arguments in variable  \$S4M_MOD_OPTS
  ## to do as you please, if you prefer.
  ##
}

### START ###
main
"
}


install_script_template () {
  modulename="$1"
  echo "#!/bin/sh
## Insert shell code to check / install your module here.
##
## Calling this script when your  module is already set up should not cause an error
## (i.e. a user can run \"s4m install <your_module/version>\" to invoke this script
## in order to fix a broken installation).
## 
## TIPS:
##
## - Test that your module is already installed and take the appropriate action
##   (NOTE: \"installed\" means that your  module has everything it needs to run without error)
## - Return a suitable exit status based on success/failure
## - Any non standard tools/software used here should be reflected in module.ini
##   (\"CommandDepends\" option)
## - Installation should be as platform independent as possible (beyond GNU coreutils
##   and Linux/*nix x86_64 i.e. the minimal requirements of S4M itself)
##
##
## Example:
##
##   An install.sh script for a module that uses the 'miniconda' module to satisfy its
##   software dependencies and runtime environment (by supplying a Conda envirionment file
##   to check / install):
##
##
## #!/bin/sh
## # Include S4M functions
## for i in \`ls \$S4M_SCRIPT_HOME/inc/*.sh\`; do . \$i; done
## # Run or rerun conda setup
## s4m_exec miniconda::setup -e ./environment.yml -m \"\$1\" \"\$2\"
## exit \$?
##
##
##  (NOTE: \"\$1\" here is \"<module/version>\" string passed automatically into install.sh by S4M,
##     and \"\$2\" is optional --force flag)
##
##"
}


Usage () {
  echo
  echo "Usage: $0 <module_path> <module_name> [<module_version>]"; echo
  echo "   NOTE: If not specified, module version will be '0.1'"; echo
  exit 1
}


main () {
  modulepath="$1"
  modulename="$2"
  modulevers="$3"
  if [ -z "$modulevers" ]; then
    modulevers="0.1"
  fi

  if [ -z "$modulepath" -o -z "$modulename" ]; then
    Usage
  fi

  if [ ! -d "$modulepath" ]; then
    s4m_error "Module path '$modulepath' does not exist!"
    exit 1
  fi
  if [ ! -w "$modulepath" ]; then
    s4m_error "Module path '$modulepath' not writeable!"
    exit 1
  fi

  ## Check module name portability (alphanumeric only etc)
  ## See 'PORTABILITY' doc for details (just the naming parts apply)
  err=`pathchk --portability "$modulename" 2>&1`
  if [ $? -ne 0 ]; then
    ## ignore "length" errors
    echo "$err" | grep length > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      s4m_error "Error: Module name '$modulename' contains non-portable characters!"
      echo "  Please use only alphanumeric and underscore (no spaces)."; echo
      exit 1
    fi
  fi

  if [ -d "$modulepath/$modulename" ]; then
    s4m_error "Module '$modulename' already exists!"
    exit 1
  fi

  ## The module directory is created here! 
  mkdir -p "$modulepath/$modulename/$modulevers"

  if [ $? -ne 0 ]; then
    s4m_error "Could not create module path '$modulepath/$modulename/$modulevers!"
    exit 1
  fi

  ini_file_template "$modulename" "$modulevers" > \
"$modulepath/$modulename/$modulevers/module.ini"
  user_script_template "$modulename" > \
"$modulepath/$modulename/$modulevers/$modulename.sh"
  install_script_template "$modulename" > \
"$modulepath/$modulename/$modulevers/install.sh"
}


## MAIN ##
main "$1" "$2" "$3"

