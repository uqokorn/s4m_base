#/bin/sh
##==========================================================================
## Module: _selftest
## Author: Othmar Korn
##
## Regression test of core functions. Self-contained (does not require any
## dependencies).
##==========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##==========================================================================

# disable "stop on error" for these tests (we want to run through them start to end)
S4M_HALT_ON_ERROR=0
export S4M_HALT_ON_ERROR

### UTIL ###

start_test () {
  testname="$1"
  S4M_TEST_COUNTER=`expr $S4M_TEST_COUNTER + 1`; export S4M_TEST_COUNTER
  echo "($S4M_TEST_COUNTER) $testname:"
}

test_pass () {
  S4M_PASS_COUNTER=`expr $S4M_PASS_COUNTER + 1`; export S4M_PASS_COUNTER
  echo "  Pass"
}

test_fail () {
  echo "Error: $1
  [FAIL]" 1>&2
}


### TESTS ###

test_mandatory_env () {
  start_test "mandatory_env"

  if [ -z "$S4M_SCRIPT_HOME" ]; then
    # technically, we should never get here - the mere fact that this module is 
    # running at all means this MUST have been set!
    test_fail "Missing required env var 'S4M_SCRIPT_HOME'"
  elif [ -z "$S4M_MODULE" ]; then
    # .. ditto with this one
    test_fail "Missing required env var 'S4M_MODULE'"
  elif [ -z "$S4M_TMP" ]; then
    test_fail "Missing required env var 'S4M_TMP'"
  elif [ -z "$S4M_DEFAULT_CONFIG" ]; then
    test_fail "Missing required env var 'S4M_DEFAULT_CONFIG'"
  elif [ -z "$S4M_MOD_CMD" ]; then
    test_fail "Missing required env var 'S4M_MOD_CMD'"
  else
    test_pass
  fi
}

test_shell_discovery () {
  start_test "shell_discovery"

  envshell=`readlink -f "$SHELL"`
  SHELL=""; export SHELL
  s4mshell=`s4m_get_shell`
  if [ "$envshell" != "$s4mshell" ]; then
    test_fail "Expecting output '$envshell' from 's4m_get_shell()'"
  else
    test_pass
  fi; export SHELL
  ## restore SHELL
  SHELL="$envshell"; export SHELL
}

test_configfile_path_exists () {
  start_test "configfile_path_exists"

  if [ ! -z "$S4M_DEFAULT_CONFIG" ]; then
    test_pass
  else
    test_fail "Environment variable 'S4M_DEFAULT_CONFIG' must not be empty"
  fi
}

test_debug_toggle_value () {
  start_test "debug_toggle"

  if [ -z "$S4M_DEBUG" ]; then
    test_fail "S4M_DEBUG environment var must be set!"
  else
    if [ "$S4M_DEBUG" = "TRUE" -o "$S4M_DEBUG" = "FALSE" ]; then
      test_pass
    else
      test_fail "S4M_DEBUG environment var must be 'TRUE' or 'FALSE'"
    fi
  fi
}

test_debug_toggle_operation () {
  start_test "debug_toggle_operation"

  OLDDEBUG="$S4M_DEBUG"
  S4M_DEBUG="TRUE"; export S4M_DEBUG

  s4m_debug "foo" 2>&1 | grep "DEBUG" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    test_fail "Debug toggle on but not printing debug message"
    return
  fi

  if ! s4m_isdebug; then
    test_fail "Failed test for TRUE debug status using s4m_isdebug()"
    return
  fi

  S4M_DEBUG="FALSE"; export S4M_DEBUG

  s4m_debug "foo" 2>&1 | grep "DEBUG" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    test_fail "Debug toggle is off but debug message was printed"
    return
  fi

  if s4m_isdebug; then
    test_fail "Failed test for FALSE debug status using s4m_isdebug()"
    return
  fi

  S4M_DEBUG="$OLDDEBUG"; export S4M_DEBUG
  test_pass
}

test_halt_on_error () {
  start_test "halt_on_error"

  OLDHALT="$S4M_HALT_ON_ERROR"
  S4M_HALT_ON_ERROR=1; export S4M_HALT_ON_ERROR
  echo "s4m_error foo > /dev/null 2>&1; echo bar" > /tmp/s4mtest.sh
  stdout=`$SHELL /tmp/s4mtest.sh`
  rm -f /tmp/s4mtest.sh

  if [ "$stdout" = "bar" ]; then
    test_fail "Halt on error is ON, but test script carried on after error"
  else
    test_pass
  fi
  S4M_HALT_ON_ERROR="$OLDHALT"; export S4M_HALT_ON_ERROR
}

test_fatal_error_exit_status () {
  start_test "fatal_error_exit_status"

  # A number <= 255
  teststat=255
  (s4m_fatal_error "fatal" $teststat > /dev/null 2>&1)
  exitstat="$?"
  if [ $exitstat -eq $teststat ]; then
    test_pass
  else
    test_fail "Fatal error not propagating custom exit status (got $exitstat when expecting $teststat)"
  fi
}

test_s4m_error () {
  start_test "s4m_error"
  stderr=`s4m_error "this is a test error" 2>&1`
  echo "$stderr" | grep -P "[0-9]{8}_[0-9]{6} _selftest\] ERROR\:" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    test_fail "If logging format (timestamp, error or module labels) changed, this might be a false positive error"
  else
    test_pass
  fi
}

test_s4m_log () {
  start_test "s4m_log"
  stdout=`s4m_log "this is a test log"`
  echo "$stdout" | grep -P "[0-9]{8}_[0-9]{6} _selftest" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    test_fail "If logging format (timestamp, error or module labels) changed, this might be a false positive error"
  else
    test_pass
  fi
}

test_s4m_warn () {
  start_test "s4m_warn"
  stderr=`s4m_warn "this is a test warning" 2>&1`
  echo "$stderr" | grep -P "[0-9]{8}_[0-9]{6} _selftest\] WARN\:" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    test_fail "If logging format (timestamp, error or module labels) changed, this might be a false positive error"
  else
    test_pass
  fi
}

test_s4m_push_messages () {
  start_test "s4m_push_messages"

  s4m_push_error "xxx yyy"
  retrieved=`s4m_pop_error 2>&1`
  ## We wouldn't normally capture the error message in general usage, but since we
  ## did for this test, in a subshell, we stil have to unset our S4M_LAST_ERROR
  ## in the current environment
  s4m_pop_error > /dev/null 2>&1

  echo "$retrieved" | grep "xxx yyy" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    test_fail "Failed to retrieve pushed error message"
    return
  fi
  if [ ! -z "$S4M_LAST_ERROR" ]; then
    test_fail "Pushed error message not cleared after pop  S4M_LAST_ERROR=[$S4M_LAST_ERROR]"
    return
  fi

  ## should return 1 on attempt to pop empty error
  if s4m_pop_error; then
    test_fail "Returned non-zero status when popping cleared error message (should return 1)"
    return
  fi

  ## NOTE: Not testing "s4m_push_warn()" or "s4m_pop_warn()" since they're functionally
  ## identical to s4m_push_error() and s4m_pop_error except for the log decoration

  test_pass
}

test_inifile_contains () {
  start_test "inifile_contains"

  testini="$S4M_THIS_MODULE/etc/test.ini"
  if [ ! -f "$testini" ]; then
    test_fail "Module '$S4M_MODULE' file 'etc/test.ini' is missing"
    return
  fi
  hasfoo=`s4m_inifile_contains "$testini" foo`
  if [ "$hasfoo" != "TRUE" ]; then
    test_fail "Failed testing for existence of known .ini file variable"
    return
  fi
  test_pass
}

test_inifile_get () {
  start_test "inifile_get"

  testini="$S4M_THIS_MODULE/etc/test.ini"
  if [ ! -f "$testini" ]; then
    test_fail "Module '$S4M_MODULE' file 'etc/test.ini' is missing"
    return
  fi
  val=`s4m_inifile_get "$testini" num`
  if [ "$val" != "1.23" ]; then
    test_fail "Failed fetching known value from .ini file (got '$val', expecting '1.23' for var 'num' in file $testini)"
    return
  fi
  test_pass
}

test_inifile_get_multiline () {
  start_test "inifile_get_multiline"

  testini="$S4M_THIS_MODULE/etc/test.ini"
  if [ ! -f "$testini" ]; then
    test_fail "Module '$S4M_MODULE' file 'etc/test.ini' is missing"
    return
  fi
  multival=`s4m_inifile_get_multiline "$testini" multi`
  expected_value="one,
two,
three"
  if [ "$multival" != "$expected_value" ]; then
    test_fail "Multi-line value does not match expected; got=[$multival], expected=[$expected_value]"
    return
  fi

  ## should also work for single line values
  singleval=`s4m_inifile_get_multiline "$testini" num`
  if [ "$val" != "1.23" ]; then
    test_fail "Failed fetching known value from .ini file (got [$val], expecting [1.23] for var 'num' in file $testini)"
    return
  fi
  test_pass
}


test_set_config_item () {
  start_test "set_config_item"

  testini="$S4M_THIS_MODULE/etc/test.ini"
  cp "$testini" "$S4M_TMP/s4mtest.ini"
  # temp override S4M_DEFAULT_CONFIG path (to temp .ini file)
  OLD_S4M_DEFAULT_CONFIG="$S4M_DEFAULT_CONFIG"
  export S4M_DEFAULT_CONFIG="$S4M_TMP/s4mtest.ini"

  # test write to known existing variable
  s4m_set_config_item "foo" "baz"
  if [ $? -ne 0 ]; then
    export S4M_DEFAULT_CONFIG="$OLD_S4M_DEFAULT_CONFIG"
    test_fail "Failed to write new value for known var in .ini file"
    return
  fi
  # test write non-existing variable (append)
  s4m_set_config_item "newvar" "x,y,z"
  xyz=`s4m_inifile_get "$S4M_DEFAULT_CONFIG" "newvar"`
  if [ "$xyz" != "x,y,z" ]; then
    export S4M_DEFAULT_CONFIG="$OLD_S4M_DEFAULT_CONFIG"
    test_fail "Failed to append new var to .ini file"
    return
  fi

  ## NOTE: No multi-line value setting support yet, but the test might look
  ##  like this:

#  multival="one,
#two,
#three"
#  s4m_set_config_item_multi "multi" "$multival"
#  multi=`s4m_inifile_get_multiline "$S4M_DEFAULT_CONFIG" "newvar"`
#  if [ "$multi" != "$multival" ]; then
#    export S4M_DEFAULT_CONFIG="$OLD_S4M_DEFAULT_CONFIG"
#    test_fail "Failed to append new multi-line var to .ini file"
#    return
#  fi

  # restore default config path
  export S4M_DEFAULT_CONFIG="$OLD_S4M_DEFAULT_CONFIG"
  test_pass
}

test_get_config_item () {
  start_test "get_config_item"

  # temp override S4M_DEFAULT_CONFIG path (to temp .ini file)
  OLD_S4M_DEFAULT_CONFIG="$S4M_DEFAULT_CONFIG"
  export S4M_DEFAULT_CONFIG="$S4M_TMP/s4mtest.ini"

  val=`s4m_get_config_item "newvar"`
  if [ "$val" != "x,y,z" ]; then
    export S4M_DEFAULT_CONFIG="$OLD_S4M_DEFAULT_CONFIG"
    test_fail "Failed to fetch known variable from .ini file (got '$val', expecting 'x,y,z' for var 'newvar' in: '$S4M_TMP/s4mtest.ini')"
    return
  fi

  # restore default config path
  export S4M_DEFAULT_CONFIG="$OLD_S4M_DEFAULT_CONFIG"
  test_pass 
}

test_s4m_rel_to_abs_path () {
  start_test "s4m_rel_to_abs_path"

  ## current relative directory "."
  ## NOTE: Removed test as we can now have module path outside s4m application path
  expect_path=`readlink -f "$S4M_MODULE_PATH/$S4M_MODULE"`
  resolved_path=`s4m_rel_to_abs_path "./.."`
  if [ "$resolved_path" != "$expect_path" ]; then
    test_fail "Resolved path '$resolved_path' (via '.') does not match expected absolute path to module '$expect_path'"
    return
  fi

  ## current relative directory via back and forward ../$S4M_MODULE
  ## NOTE: Removed test as we can now have module path outside s4m application path
  expect_path=`readlink -f "$S4M_MODULE_PATH/$S4M_MODULE"`
  resolved_path=`s4m_rel_to_abs_path "../../$S4M_MODULE"`
  if [ "$resolved_path" != "$expect_path" ]; then
    test_fail "Resolved path '$resolved_path' (via '../$S4M_MODULE') does not match expected absolute path to module '$expect_path'"
    return
  fi

  test_pass
}

test_s4m_in_array_1 () {
  start_test "s4m_in_array_1"
  haystack="one two three"
  needle="two"
  if ! s4m_in_array $needle "$haystack"; then
    test_fail "Token search in space-separated string failed"
    return
  fi
  test_pass
}

test_s4m_in_array_2 () {
  start_test "s4m_in_array_2"
  haystack="one,two,three"
  needle="two"
  if ! s4m_in_array $needle "$haystack"; then
    test_fail "Token search in comma-separated string failed"
    return
  fi
  test_pass
}

test_s4m_in_array_3 () {
  start_test "s4m_in_array_3"
  haystack="phone,two,three"
  needle="one"
  if s4m_in_array $needle "$haystack"; then
    test_fail "Token search matched string and not element in array"
    return
  fi
  test_pass
}

test_s4m_in_array_4 () {
  start_test "s4m_in_array_4"
  haystack="one
two
three"
  needle="two"
  if ! s4m_in_array $needle "$haystack"; then
    test_fail "Token search failed to match element in newline separated string"
    return
  fi
  test_pass
}

test_s4m_in_array_5 () {
  start_test "s4m_in_array_5"
  haystack="one
two
three"
  needle="two"
  if ! s4m_in_array $needle $haystack; then
    test_fail "Token search failed to match element in non-quoted shell array"
    return
  fi
  test_pass
}

test_s4m_in_array_6 () {
  start_test "s4m_in_array_6"
  haystack="one,two,three"
  needle="two,"
  if s4m_in_array $needle $haystack; then
    test_fail "Token search incorrectly matched token with attached separator"
    return
  fi
  test_pass
}

test_s4m_in_array_7 () {
  start_test "s4m_in_array_7"
  haystack="one,two,three"
  needle="two,three"
  if s4m_in_array $needle $haystack; then
    test_fail "Token search incorrectly matched multiple tokens with attached separator"
    return
  fi
  test_pass
}

test_s4m_get_module_db_file () {
  start_test "s4m_get_module_db_file"
  ## temp override module DB
  S4M_MODULE_DB="$S4M_THIS_MODULE/etc/moduledata"

  selftest_module_db_file=`s4m_get_module_db_file`
  if [ ! -f "$selftest_module_db_file" ]; then
    test_fail "Module DB file path does not match expected"
    return
  fi
  test_pass
}

test_s4m_module_db_get () {
  start_test "s4m_module_db_get"
  ## temp override module DB
  S4M_MODULE_DB="$S4M_THIS_MODULE/etc/moduledata"

  foo=`s4m_module_db_get "foo"`
  if [ $foo != "bar" ]; then
    test_fail "Module database value for 'foo' is '$foo', expecting 'bar'"
    return
  fi
  test_pass
}

test_s4m_module_db_has_key () {
  start_test "s4m_module_db_has_key"
  ## temp override module DB
  S4M_MODULE_DB="$S4M_THIS_MODULE/etc/moduledata"

  if s4m_module_db_has_key "bar"; then
    test_fail "Module database reporting existence of key 'bar' when it should not exist"
    return
  fi
  if ! s4m_module_db_has_key "foo"; then
    test_fail "Module database reporting key 'foo' not found, when it should exist"
    return
  fi
  test_pass
}

test_s4m_module_version () {
  start_test "s4m_module_version"

  if ! s4m_module_version > /dev/null; then
    test_fail "Failed to get version for current (implicit) module"
    return
  fi
  if ! s4m_module_version "$S4M_MODULE" > /dev/null; then
    test_fail "Failed to get version for current (explicity supplied) module name"
    return
  fi
  test_pass
}

test_module_args_capture () {
  start_test "module_args_capture"

  ##
  ## [1] - simple test
  ##
  set -- "--foo fighters --bar fights"

  #echo "DEBUG: Auto array = [$@]"

  if ! s4m_capture_module_args $@; then
    test_fail "Failed to capture module args"
    return
  fi
  
  ## should now have exported variables called:
  ##    <thismodule>_foo
  ##    <thismodule>_bar
  env | grep ${S4M_MODULE}_foo > /dev/null 2>&1
  hasfoo=$?
  env | grep ${S4M_MODULE}_bar > /dev/null 2>&1
  hasbar=$?

  if [ $hasfoo -ne 0 -o $hasbar -ne 0 ]; then
    test_fail "One or more module args were not automatically set!"
    return
  else
    fooval=`eval echo \\$"${S4M_MODULE}_foo"`
    barval=`eval echo \\$"${S4M_MODULE}_bar"`
    #echo "DEBUG: --foo=[$fooval], --bar=[$barval]"
    #echo "DEBUG: _selftest_foo=[$_selftest_foo], _selftest_bar=[$_selftest_bar]"
    if [ "$fooval" != "fighters" -o "$barval" != "fights" ]; then
      test_fail "Args were not captured correctly!"
      return
    fi
  fi

  ##
  ## [2] - Prepend toggle arg
  ##
  unset ${S4M_MODULE}_toggle
  unset ${S4M_MODULE}_foo
  unset ${S4M_MODULE}_bar
  set -- "--toggle --foo fighters --bar fights"
  if ! s4m_capture_module_args $@; then
    test_fail "Failed to capture module args"
    return
  fi
  toggleval=`eval echo \\$"${S4M_MODULE}_toggle"`
  if [ "$toggleval" != "true" ]; then
    test_fail "Failed to capture prepended toggle (boolean) arg correctly!"
    return
  fi

  ##
  ## [3] - Append toggle arg
  ##
  unset ${S4M_MODULE}_toggle
  unset ${S4M_MODULE}_foo
  unset ${S4M_MODULE}_bar
  set -- "--foo fighters --bar fights --toggle"
  if ! s4m_capture_module_args $@; then
    test_fail "Failed to capture module args"
    return
  fi
  toggleval=`eval echo \\$"${S4M_MODULE}_toggle"`
  if [ "$toggleval" != "true" ]; then
    test_fail "Failed to capture appended toggle (boolean) arg correctly!"
    return
  fi

  ##
  ## [4] - Short flags
  ##
  unset ${S4M_MODULE}_toggle
  unset ${S4M_MODULE}_foo
  unset ${S4M_MODULE}_bar
  set -- "-t -f fighters -b fights"
  if ! s4m_capture_module_args $@; then
    test_fail "Failed to capture module args"
    return
  fi
  toggleval=`eval echo \\$"${S4M_MODULE}_toggle"`
  fooval=`eval echo \\$"${S4M_MODULE}_foo"`
  barval=`eval echo \\$"${S4M_MODULE}_bar"`
  if [ "$fooval" != "fighters" -o "$barval" != "fights" -o "$toggleval" != "true" ]; then
    test_fail "Args were not captured correctly when given as short flags!"
    return
  fi

  ##
  ## [5] - Mix of short and long flags
  ##
  unset ${S4M_MODULE}_toggle
  unset ${S4M_MODULE}_foo
  unset ${S4M_MODULE}_bar
  set -- "-t -f fighters --bar fights"
  if ! s4m_capture_module_args $@; then
    test_fail "Failed to capture module args"
    return
  fi
  toggleval=`eval echo \\$"${S4M_MODULE}_toggle"`
  fooval=`eval echo \\$"${S4M_MODULE}_foo"`
  barval=`eval echo \\$"${S4M_MODULE}_bar"`
  if [ "$fooval" != "fighters" -o "$barval" != "fights" -o "$toggleval" != "true" ]; then
    test_fail "Args were not captured correctly when given as a mix of short and long flags!"
    return
  fi

  test_pass
}

test_s4m_routing () {
  start_test "s4m_routing"

  ## Should fail if we route to non-existing function
  s4m_route "selftest" "this_function_does_not_exist" > /dev/null 2>&1
  if [ $? -ne 1 ]; then
    test_fail "Module command -> function() route failed to return error status on unknown function!"
    return
  fi

  ## A route that should work - we need to disable post-route exiting though
  S4M_EXIT_ON_ROUTE="FALSE"; export S4M_EXIT_ON_ROUTE
  s4m_route "selftest" "showtests_handler" > /dev/null
  if [ $? -ne 0 ]; then
    test_fail "Module command -> function() route returned non-zero status for valid function!"
    return
  fi

  ## Verify that command options (flags) received by handler
  ## Post-route exiting should still be turned off for this test.
  OLD_S4M_MOD_OPTS="$S4M_MOD_OPTS"
  S4M_MOD_OPTS="--foo=bar"; export S4M_MOD_OPTS
  if ! s4m_route "selftest" "verify_args_handler"; then
    test_fail "Command options (flags) not passed to command handler function!"
    S4M_MOD_OPTS="$OLD_S4M_MOD_OPTS"; export S4M_MOD_OPTS
    S4M_EXIT_ON_ROUTE="TRUE"; export S4M_EXIT_ON_ROUTE
    return
  fi
  S4M_MOD_OPTS="$OLD_S4M_MOD_OPTS"; export S4M_MOD_OPTS
  S4M_EXIT_ON_ROUTE="TRUE"; export S4M_EXIT_ON_ROUTE

  test_pass
}

## Assumes we actually *do* have an Internet connection at runtime
##
test_s4m_has_internet_connection () {
  start_test "s4m_has_internet_connection"

  if s4m_has_internet_connection; then
    test_pass
    return
  fi

  test_fail
}

## Check that "sort -V" and "sort -t. -n" give the same results
## (if -V is supported for sort found in current env)
##
test_GNU_version_sort_compatibility () {
  start_test "GNU_version_sort_compatibility"

  versions="0.1
0.2
1.0
1.11
1.3
0.22"
  echo "$versions" | sort -V > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    vsort_results=`echo "$versions" | sort -V`
    ## NOTE: Stemformatics modules don't use 3 version levels e.g. x.y.z, but in case somebody
    ##  out there wants to, then this would allow the versions to be sorted correctly.
    nsort_results=`echo "$versions" | sort -n -t. -k1,1 -k2,2 -k3,3`
    if [ "$vsort_results" != "$nsort_results" ]; then
      test_fail "sort -V  and  sort -n  give different results!"
      return
    fi
  fi

  test_pass
}


### HELPERS (not tests) ###

## used to test args passing to custom user funcs
verify_args_handler () {
  if [ -z "$@" ]; then
    return 1
  fi
  return 0
}


## command route handler for "showtests"
showtests_handler () {

  #echo "DEBUG: showtests_handler(): args=[$@]" 1>&2

  ## pull our test functions out from the environment and return their names
  test_funcs=`set | grep -P "^test_.*\(\)\s*$" | cut -d' ' -f 1`
  echo "$test_funcs"
}


selftest_handler () {
  ### Run main tests ###

  S4M_TEST_COUNTER=0
  S4M_PASS_COUNTER=0
  export S4M_TEST_COUNTER S4M_PASS_COUNTER

  ## We could automate the tests like the following, but we can't guarantee the test
  ## order we want without renaming all the test functions...
  #tests=`showtests_handler`
  #for testfunc in $tests
  #do
  #  eval "$testfunc"
  #done

  ## Run tests in the order we want
  echo "Running tests.."
  ## CORE
  test_mandatory_env
  test_shell_discovery
  test_configfile_path_exists
  test_debug_toggle_value
  test_debug_toggle_operation
  ## Temp disable this next test until we know if its actually testable due to
  ## "exit" mechanics / subshells etc.
  #test_halt_on_error
  test_fatal_error_exit_status
  test_s4m_error
  test_s4m_log
  test_s4m_warn
  test_s4m_push_messages
  test_inifile_contains
  test_inifile_get
  test_inifile_get_multiline
  test_set_config_item
  test_get_config_item
  test_s4m_rel_to_abs_path
  test_s4m_in_array_1
  test_s4m_in_array_2
  test_s4m_in_array_3
  test_s4m_in_array_4
  test_s4m_in_array_5
  test_s4m_in_array_6
  test_s4m_in_array_7
  test_GNU_version_sort_compatibility

  ## MODULES
  test_module_args_capture
  test_s4m_get_module_db_file
  test_s4m_module_db_get
  test_s4m_module_db_has_key
  test_s4m_module_version
  test_s4m_routing
  test_s4m_has_internet_connection

  echo
  echo "Test Summary:"
  echo "--------------"
  echo "
  Passed $S4M_PASS_COUNTER of $S4M_TEST_COUNTER tests.
  "
}


main () {

  ### DEBUG ###
  echo "DEBUG: S4M_MOD_CMD=[$S4M_MOD_CMD]" 1>&2
  echo "DEBUG: S4M_MOD_OPTS=[$S4M_MOD_OPTS]" 1>&2
  ### END ###

  s4m_route "selftest" "selftest_handler"
  s4m_route "showtests" "showtests_handler"

  ## .. cannot get here unless given command has no routes,
  ## or post-route exiting is disabled (ie. $S4M_EXIT_ON_ROUTE=="FALSE")

}

### START ###
main
